/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React from 'react'
import PageLayout from './src/templates/PageLayout'

export const wrapPageElement = ({ element }) => <PageLayout>{element}</PageLayout>
