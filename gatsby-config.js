module.exports = {
  siteMetadata: {
    // siteUrl: 'https://www.yourdomain.tld',
    // title: 'PlayerLayer',
  },
  plugins: [
    {
      resolve: 'gatsby-source-wordpress',
      options: {
        url: 'https://cms.athlo.app/graphql',
      },
    },
    'gatsby-plugin-styled-components',
    'gatsby-plugin-image',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    'gatsby-plugin-tsconfig-paths',
    {
      resolve: 'gatsby-plugin-facebook-pixel',
      options: {
        pixelId: '194337249490280',
      },
    },
    {
      resolve: 'gatsby-plugin-alias-imports',
      options: {
        alias: {
          '@assets': './src/assets',
          '@images': './src/images',
          '@components': './src/components',
          '@context': './src/context',
          '@helpers': './src/helpers',
          '@hooks': './src/hooks',
          '@pages': './src/pages',
          '@sections': './src/sections',
          '@styles': './src/styles',
          '@themes': './src/themes',
          '@utils': './src/utils',
        },
      },
    },
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'Athlo | Gym Membership. Upcycled.',
        short_name: 'Athlo',
        start_url: '/',
        background_color: '#f5b9b3',
        theme_color: '#f5b9b3',
        display: 'minimal-ui',
        icon: 'src/assets/favicon/favicon.png', // This path is relative to the root of the site.
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: './src/images/',
      },
      __key: 'images',
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /assets/, // See below to configure properly
        },
      },
    },
  ],
}
