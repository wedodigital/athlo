import React from 'react'
import Enzyme from 'enzyme'

// define globals
global.React = React
global.mount = () => {
  console.error('DO NOT USE!')
}
global.shallow = Enzyme.shallow
global.render = Enzyme.render
global.snapshot = () => {
  console.error('DO NOT USE')
}
