import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledContainerProps } from './Container.style.types'

export const Container = styled.div((props: StyledContainerProps): FlattenSimpleInterpolation => css`
  width: 86%;
  margin: 0 auto;
  max-width: 1240px;

  ${props.containerWidth === 'wide' && css`
    max-width: 1600px;
  `}

  ${props.containerWidth === 'mid' && css`
    max-width: 1000px;
  `}

  ${props.containerWidth === 'narrow' && css`
    max-width: 800px;
  `}
`)
