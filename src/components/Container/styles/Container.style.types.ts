import { Theme } from '@themes/athloTheme/athloTheme.types'

import { ContainerProps } from '../Container.types'

export interface StyledContainerProps {
  theme: Theme;
  containerWidth: ContainerProps['width']
}
