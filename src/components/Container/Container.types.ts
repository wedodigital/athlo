import { ReactNode } from 'react'

export interface ContainerProps {
  /**
   * React children
   * */
  children: ReactNode;
  /**
   * Container width
   * */
  width?: 'default' | 'wide' | 'mid' | 'narrow'
}
