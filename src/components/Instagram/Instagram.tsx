import React, { ReactElement, FC } from 'react'
import InstagramFeed  from 'react-ig-feed'

import Section from '@components/Section'

import * as Styled from './styles/Instagram.style'

const Instagram: FC = (): ReactElement => {
  return (
    <Styled.Instagram>
      <Section paddingLevel={1} containerWidth='wide'>
        <Styled.Feed>
          <Styled.Feature>
            <InstagramFeed
              token='IGQVJYdFZAtRlgxLUtrNDd1SHRvZAWc2RHQzUnEyWl9kcmJONWJNTktkS1hUcGlvXzJvYU52Tm9laU1LSzliYmFtc1Q2MGx5QmNMc2E0bXctc2cyVDNzVG42RFRMMEZAnS3JfYlVsM0RKXzY0TFBkbFRCSQZDZD'
              counter='1'
            />
          </Styled.Feature>
          <Styled.MainFeed>
            <InstagramFeed
              token='IGQVJYdFZAtRlgxLUtrNDd1SHRvZAWc2RHQzUnEyWl9kcmJONWJNTktkS1hUcGlvXzJvYU52Tm9laU1LSzliYmFtc1Q2MGx5QmNMc2E0bXctc2cyVDNzVG42RFRMMEZAnS3JfYlVsM0RKXzY0TFBkbFRCSQZDZD'
              counter='7'
            />
          </Styled.MainFeed>
        </Styled.Feed>
      </Section>
    </Styled.Instagram>
  )
}

export default Instagram
