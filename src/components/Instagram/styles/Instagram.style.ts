import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledInstagramProps } from './Instagram.style.types'

export const Instagram = styled.div((props: StyledInstagramProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.white};
`)

export const Feed = styled.div((props: StyledInstagramProps): FlattenSimpleInterpolation => css`
  display: flex;
  gap: ${props.theme.spacing.fixed[2]}px;
  flex-direction: column;

  ${props.theme.mixins.respondTo.md(css`
    flex-direction: row;
  `)}

  img,
  video {
    display: block;
    max-width: 100%;
  }
`)

export const Feature = styled.div((props: StyledInstagramProps): FlattenSimpleInterpolation => css`
  ${props.theme.mixins.respondTo.md(css`
    flex-basis: 630px;
  `)};

  & > div {
    & > div {
      div {
        display: none;
      }
    }
  }
`)

export const MainFeed = styled.div((props: StyledInstagramProps): FlattenSimpleInterpolation => css`
  ${props.theme.mixins.respondTo.md(css`
    flex-basis: calc(1600px - (630px + ${props.theme.spacing.fixed[2]}px));
  `)}

  & > div {
    display: flex;
    gap: ${props.theme.spacing.fixed[2]}px;
    flex-wrap: wrap;

    & > div {
      width: calc((100% - ${props.theme.spacing.fixed[2] * 2}px) / 3);
      aspect-ratio: 1 / 1;

      div {
        display: none;
      }

      &:first-child {
        display: none;
      }
    }
  }
`)
