import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledInstagramProps {
  theme: Theme;
}
