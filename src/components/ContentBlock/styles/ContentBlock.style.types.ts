import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledContentBlockProps {
  theme: Theme;
}
