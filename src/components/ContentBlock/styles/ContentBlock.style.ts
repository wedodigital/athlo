import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledContentBlockProps } from './ContentBlock.style.types'

export const ContentBlock = styled.div((props: StyledContentBlockProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.white};
  padding: ${props.theme.spacing.fixed[8]}px;
  font-size: ${props.theme.typography.paragraph[2].fontSize};
`)
