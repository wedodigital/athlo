import { ReactNode } from 'react'

export interface ContentBlockProps {
  /**
   * React children
   * */
  children: ReactNode;
}
