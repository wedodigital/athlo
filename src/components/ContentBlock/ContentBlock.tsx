import React, { ReactElement, FC } from 'react'

import * as Styled from './styles/ContentBlock.style'

import { ContentBlockProps } from './ContentBlock.types'
import RawHtmlWrapper from '@components/RawHtmlWrapper/RawHtmlWrapper'
import Container from '@components/Container/Container'

const ContentBlock: FC<ContentBlockProps> = ({
  content,
}: ContentBlockProps): ReactElement => {
  return (
    <Styled.ContentBlock>
      <Container width='narrow'>
        <RawHtmlWrapper content={content} />
      </Container>
      
    </Styled.ContentBlock>
  )
}

export default ContentBlock
