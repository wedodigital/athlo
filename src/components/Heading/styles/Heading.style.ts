import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledHeadingProps } from './Heading.style.types'

export const Heading = styled.h2((props: Omit<StyledHeadingProps, 'highlightText'>): FlattenSimpleInterpolation => css`
  font-size: ${props.theme.typography.heading[props.size].fontSizeMobile};
  line-height: ${props.theme.typography.heading[props.size].lineHeightMobile};
  font-weight: 400;
  text-align: ${props.justify};

  ${props.weight === 2 && css`
    font-weight: 600;
  `}

  ${props.weight === 3 && css`
    font-weight: 700;
  `}

  ${props.theme.mixins.respondTo.md(css`
    font-size: ${props.theme.typography.heading[props.size].fontSize};
    line-height: ${props.theme.typography.heading[props.size].lineHeight};
  `)}

  ${!props.noMargin && css`
    margin-bottom: ${props.theme.spacing.fixed[2]}px;

    ${props.size > 4 && css`
      margin-bottom: ${props.theme.spacing.fixed[4]}px;
    `}
  `}

  ${props.appearance === 'primary' && css`
    color: ${props.theme.colours.darkGrey};

    ${props.inverse && css`
      color: ${props.theme.colours.white};
    `}
  `}

  ${props.appearance === 'secondary' && css`
    color: ${props.theme.colours.darkBlue};

    ${props.inverse && css`
      color: ${props.theme.colours.white};
    `}
  `}

  &:last-child {
    margin-bottom: 0;
  }
`)

export const HighlightText = styled.span((props: Pick<StyledHeadingProps, 'theme' | 'highlightStyle' | 'appearance' | 'inverse'>): FlattenSimpleInterpolation => css`
  color: ${props.theme.colours.midBlue};
  font-weight: 600;

  ${props.appearance === 'primary' && css`
    color: ${props.theme.colours.midBlue};

    ${props.inverse && css`
      color: ${props.theme.colours.midBlue};
    `}
  `}

  ${props.appearance === 'secondary' && css`
    color: ${props.theme.colours.midBlue};

    ${props.inverse && css`
      color: ${props.theme.colours.black};
    `}
  `}

  ${props.highlightStyle === 'block' && css`
    display: block;
  `}
`)
