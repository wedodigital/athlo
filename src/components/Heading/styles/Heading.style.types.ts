import { Theme } from '@themes/athloTheme/athloTheme.types'

import { HeadingProps } from '../Heading.types'

export interface StyledHeadingProps {
  theme: Theme
  level: HeadingProps['level']
  size: HeadingProps['size']
  noMargin: HeadingProps['noMargin']
  inverse: HeadingProps['inverse']
  weight: HeadingProps['weight']
  appearance: HeadingProps['appearance']
  highlightStyle: HeadingProps['highlightStyle']
}
