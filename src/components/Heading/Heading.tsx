import React, { FC, ReactElement } from 'react'

import { HeadingProps } from './Heading.types'

import * as Styled from './styles/Heading.style'

const Heading: FC<HeadingProps> = ({
  text,
  level = 2,
  size = 2,
  noMargin = false,
  inverse = false,
  weight = 1,
  highlightText,
  appearance = 'primary',
  highlightStyle = 'inline',
  justify,
}: HeadingProps): ReactElement => {
  return (
    <Styled.Heading
      as={`h${level}` as React.ElementType}
      size={size}
      noMargin={noMargin}
      inverse={inverse}
      appearance={appearance}
      weight={weight}
      justify={justify}
    >
      {text}
      <If condition={highlightText}>
        <Styled.HighlightText highlightStyle={highlightStyle} appearance={appearance} inverse={inverse}>{highlightText}</Styled.HighlightText>
      </If>
    </Styled.Heading>
  )
}

export default Heading
