import { HeadingKeys } from '@themes/athloTheme/constants/typography.types'

export interface HeadingProps {
  text: string | string[]
  level?: 1 | 2 | 3 | 4 | 5
  size?: HeadingKeys
  noMargin?: boolean
  inverse?: boolean
  weight?: 1 | 2 | 3
  highlightText?: string
  appearance?: 'primary' | 'secondary'
  highlightStyle?: 'block' | 'inline'
}
