import React, { FC, ReactElement, useContext, useState } from 'react'
import Headroom from 'react-headroom'

import Logo from '@components/Logo'
import Container from '@components/Container'
import Button from '@components/Button'
import MatchMedia from '@components/MatchMedia'
import Navigation from '@components/Navigation'

import Menu from '@assets/svg/menu.svg'

import ModalContext from '@context/ModalContext'
import { ModalContextProps } from '@context/ModalContext.types'

import * as Styled from './styles/Header.style'

import { HeaderProps, HeaderState } from './Header.types'

const Header: FC<HeaderProps> = (): ReactElement => {
  const [headerStyle, setHeaderStyle] = useState('light' as HeaderState['headerStyle'])
  const modalContext = useContext(ModalContext) as ModalContextProps

  return (
    <Styled.Header>
      <Headroom
        onPin={() => setHeaderStyle('dark')}
        onUnfix={() => setHeaderStyle('light')}
      >
        <Container>
          <MatchMedia breakpoint='sm'>
            <Logo logoStyle={headerStyle} size={1} />
            <Styled.FlyoutTrigger onClick={() => modalContext.toggleFlyout()}>
              <Menu />
            </Styled.FlyoutTrigger>
          </MatchMedia>
          <MatchMedia breakpoint='md' andAbove>
            <Logo logoStyle={headerStyle} />
            <Navigation inverse={headerStyle === 'light'} />
            <Button
              href='https://apps.apple.com/gb/app/athlo/id1585635637'
              text='Download Now'
              appearance='secondary'
              inverse={headerStyle === 'light'}
            />
          </MatchMedia>
        </Container>
      </Headroom>
    </Styled.Header>
  )
}

export default Header
