export interface HeaderProps {
  unpin?: number
}

export interface HeaderState {
  headerStyle: 'light' | 'dark'
}
