import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledHeaderProps } from './Header.style.types'

export const Header = styled.div((props: StyledHeaderProps): FlattenSimpleInterpolation => css`
  z-index: 20;
  position: fixed;
  width: 100%;
  top: 0;
  left: 0;
  pointer-events: none;

  [class*=Container] {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  .headroom {
    padding: ${props.theme.spacing.fixed[3]}px 0;
    pointer-events: auto;

    &::after {
      content: '';
      display: block;
      background: ${props.theme.colours.white};
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      transition: 0.4s all ease;
    }

    &--unfixed {
      padding: ${props.theme.spacing.fixed[3]}px 0;

      ${props.theme.mixins.respondTo.md(css`
        padding: ${props.theme.spacing.fixed[6]}px 0;
      `)}

      &::after {
        background: linear-gradient(to bottom, rgba(0, 0, 0, 0.8) 0%, rgba(0, 0, 0, 0) 100%);
      }
    }

    & > * {
      position: relative;
      z-index: 20;
    }
  }
`)

export const FlyoutTrigger = styled.button((props: StyledHeaderProps): FlattenSimpleInterpolation => css`
  background: none;
  color: ${props.theme.colours.midBlue};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  text-decoration: none;
  font-weight: 700;
  border: none;
  cursor: pointer;
  font-family: ${props.theme.typography.fontFamily};
  width: ${props.theme.spacing.fixed[4]}px;
  height: ${props.theme.spacing.fixed[4]}px;
  transition: 0.4s all ease;
  flex-wrap: wrap;

  & > svg {
    width: ${props.theme.spacing.fixed[3]}px;
    height: ${props.theme.spacing.fixed[3]}px;

    * {
      fill: ${props.theme.colours.white};
    }
  }

  & > span {
    display: block;
  }
`)
