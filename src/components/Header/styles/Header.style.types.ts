import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledHeaderProps {
  theme: Theme;
}
