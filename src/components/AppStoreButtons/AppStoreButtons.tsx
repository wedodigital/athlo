import React, { ReactElement, FC } from 'react'

import AppStore from '@assets/svg/app-store.svg'
import GooglePlay from '@assets/svg/google-play.svg'

import * as Styled from './styles/AppStoreButtons.style'

import { AppStoreButtonsProps } from './AppStoreButtons.types'

const AppStoreButtons: FC<AppStoreButtonsProps> = ({
  appStore,
  googlePlay,
  justify = 'center'
}: AppStoreButtonsProps): ReactElement => {
  return (
    <Styled.AppStoreButtons justify={justify}>
      <If condition={appStore}>
        <Styled.AppStoreButton href={appStore}>
          <AppStore />
        </Styled.AppStoreButton>
      </If>
      <If condition={googlePlay}>
        <Styled.AppStoreButton href={googlePlay}>
          <GooglePlay />
        </Styled.AppStoreButton>
      </If>
    </Styled.AppStoreButtons>
  )
}

export default AppStoreButtons
