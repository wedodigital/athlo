export interface AppStoreButtonsProps {
  appStore?: string
  googlePlay?: string
  justify?: 'left' | 'center'
}
