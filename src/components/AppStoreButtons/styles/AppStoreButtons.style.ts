import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledAppStoreButtonsProps } from './AppStoreButtons.style.types'

export const AppStoreButtons = styled.div((props: StyledAppStoreButtonsProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: ${props.theme.spacing.fixed[3]}px;

  ${props.justify === 'left' && css`
    justify-content: flex-start;
  `}
`)

export const AppStoreButton = styled.a((): FlattenSimpleInterpolation => css`
  display: block;
  height: 40px;
`)
