import { Theme } from '@themes/athloTheme/athloTheme.types'

import { AppStoreButtonsProps } from '../AppStoreButtons.types'

export interface StyledAppStoreButtonsProps {
  theme: Theme;
  justify: AppStoreButtonsProps['justify']
}
