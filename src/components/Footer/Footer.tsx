import React, { ReactElement, FC } from 'react'
import { useStaticQuery, graphql, Link } from 'gatsby'
import dayjs from 'dayjs'

import Section from '@components/Section'
import Logo from '@components/Logo'
import MatchMedia from '@components/MatchMedia'

import * as Styled from './styles/Footer.style'

const Footer: FC = (): ReactElement => {
  const footerContent = useStaticQuery(graphql`
    query footer {
      wpPage(databaseId: {eq: 2}) {
        footerContent {
          socialLinks {
            title
            url
          }
          privacyPolicy {
            localFile {
              url
            }
          }
          termsConditions {
            localFile {
              url
            }
          }
        }
      }
    }
  `)

  return (
    <Styled.Footer>
      <Section>
        <Styled.LogoWrapper>
          <Logo logoStyle='dark' size={1} />
        </Styled.LogoWrapper>
        <Styled.FooterContent>
          <Styled.SocialLinks>
            {
              footerContent.wpPage.footerContent.socialLinks.map((socialLink: {url: string; title: string}, index) => {
                return (
                  <Styled.FooterItem key={index}><a href={socialLink.url}>{socialLink.title}</a></Styled.FooterItem>
                )
              })
            }
            <MatchMedia breakpoint='md' andAbove>
              <Styled.FooterItem><Link to='/privacy-policy'>Privacy Policy</Link></Styled.FooterItem>
              <Styled.FooterItem><a href={footerContent.wpPage.footerContent.termsConditions.localFile.url}>Terms & Conditions</a></Styled.FooterItem>
            </MatchMedia>
          </Styled.SocialLinks>
          <Styled.Legals>
            <MatchMedia breakpoint='sm'>
              <Styled.FooterItem><a href={footerContent.wpPage.footerContent.privacyPolicy.localFile.url}>Privacy Policy</a></Styled.FooterItem>
              <Styled.FooterItem><a href={footerContent.wpPage.footerContent.termsConditions.localFile.url}>Terms & Conditions</a></Styled.FooterItem>
            </MatchMedia>
            <Styled.FooterItem>Athlo {dayjs().year()} © all rights reserved.</Styled.FooterItem>
          </Styled.Legals>
        </Styled.FooterContent>
      </Section>
    </Styled.Footer>
  )
}

export default Footer
