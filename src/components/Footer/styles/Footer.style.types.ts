import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledFooterProps {
  theme: Theme;
}
