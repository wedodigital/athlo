import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledFooterProps } from './Footer.style.types'

export const Footer = styled.div((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.white};

  [class*=Container] {
    ${props.theme.mixins.respondTo.md(css`
      display: flex;
      align-items: center;
    `)};
  }
`)

export const FooterContent = styled.div((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  ${props.theme.mixins.respondTo.md(css`
    display: flex;
    gap: ${props.theme.spacing.fixed[4]}px;
    margin-left: auto;
  `)}
`)

export const LogoWrapper = styled.div((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[2]}px;

  ${props.theme.mixins.respondTo.md(css`
    margin-bottom: 0;
  `)}
`)

export const SocialLinks = styled.div((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  display: flex;
  gap: ${props.theme.spacing.fixed[2]}px;
  margin-bottom: ${props.theme.spacing.fixed[2]}px;

  ${props.theme.mixins.respondTo.md(css`
    gap: ${props.theme.spacing.fixed[4]}px;
    margin-bottom: 0;
  `)}
`)

export const Legals = styled.div((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  display: flex;
  gap: ${props.theme.spacing.fixed[1]}px;

  a {
    display: inline-block;
    border-right: 1px solid ${props.theme.colours.midGrey};
    padding-right: ${props.theme.spacing.fixed[1]}px;
  }
`)

export const FooterItem = styled.div((props: StyledFooterProps): FlattenSimpleInterpolation => css`
  font-size: ${props.theme.typography.paragraph[1].fontSize};
  line-height: ${props.theme.typography.paragraph[1].lineHeight};

  a {
    color: ${props.theme.colours.midGrey};
    text-decoration: none;
  }
`)
