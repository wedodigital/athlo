import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledFlyoutNavigationProps {
  theme: Theme
  isOpen: boolean
}
