import styled, { css, FlattenSimpleInterpolation } from 'styled-components'
import { Link } from 'gatsby'

import { StyledFlyoutNavigationProps } from './FlyoutNavigation.style.types'

export const FlyoutNavigation = styled.ul((): FlattenSimpleInterpolation => css`
  li {
    border-bottom: 1px solid #ccc;
  }
`)

export const NavLink = styled(Link)((props: StyledFlyoutNavigationProps): FlattenSimpleInterpolation => css`
  display: flex;
  padding: ${props.theme.spacing.fixed[2]}px 0;
  align-items: center;
  justify-content: space-between;
  background: none;
  border: none;
  width: 100%;
  color: ${props.theme.colours.darkGrey};
  text-decoration: none;
  font-family: ${props.theme.typography.fontFamily};
  font-size: ${props.theme.typography.heading[1].fontSize};

  & > svg {
    width: ${props.theme.spacing.fixed[3]}px;
    height: ${props.theme.spacing.fixed[3]}px;
    transition: 0.4s all ease;

    * {
      fill: ${props.theme.colours.midBlue};
    }
  }

  ${props.isOpen && css`
    & > svg {
      transform: rotate(180deg);
    }
  `}
`)
