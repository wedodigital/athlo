import React, { ReactElement, FC, useState, useContext } from 'react'
import AnimateHeight from 'react-animate-height'

import AngleRight from '@assets/svg/angle-right.svg'
import AngleDown from '@assets/svg/angle-down.svg'

import PreRegistration from '@components/PreRegistration'
import GymPartner from '@components/GymPartner'

import ModalContext from '@context/ModalContext'
import { ModalContextProps } from '@context/ModalContext.types'

import * as Styled from './styles/FlyoutNavigation.style'

import { FlyoutNavigationProps } from './FlyoutNavigation.types'

const FlyoutNavigation: FC<FlyoutNavigationProps> = ({
  // add props
}: FlyoutNavigationProps): ReactElement => {
  const [isOpen, setIsOpen] = useState('pre-reg')
  const modalContext = useContext(ModalContext) as ModalContextProps

  const navLinks = [
    {
      title: 'Home',
      url: '/'
    },
    {
      title: 'Gym Partners',
      url: '/partners'
    },
    {
      title: 'Get in Touch',
      url: '/support'
    },
  ]

  return (
    <Styled.FlyoutNavigation>
      {
        navLinks.map((navLink, index) => {
          return (
            <li key={index}>
              <Styled.NavLink to={navLink.url} onClick={() => modalContext.toggleFlyout()} isOpen={false}>
                <span>{navLink.title}</span>
                <AngleRight />
              </Styled.NavLink>
            </li>
          )
        })
      }
      <li>
        <Styled.NavLink
          as='button'
          isOpen={isOpen === 'partner'}
          onClick={() => setIsOpen(isOpen === 'partner' ? '' : 'partner')}
        >
          <span>Become a Gym Partner</span>
          <AngleDown />
        </Styled.NavLink>
        <AnimateHeight
          duration={ 500 }
          height={isOpen === 'partner' ? 'auto' : 0}
        >
          <GymPartner showTitle={false} />
        </AnimateHeight>
      </li>
    </Styled.FlyoutNavigation>
  )
}

export default FlyoutNavigation
