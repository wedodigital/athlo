import { ReactNode } from 'react'

export interface FlyoutNavigationProps {
  /**
   * React children
   * */
  children: ReactNode;
}
