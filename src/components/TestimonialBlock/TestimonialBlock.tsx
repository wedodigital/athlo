import React, { ReactElement, FC } from 'react'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import Section from '@components/Section'
import Heading from '@components/Heading'
import Arrow from '@components/Arrow'

import Testimonial from './components/Testimonial'

import * as Styled from './styles/TestimonialBlock.style'

import { TestimonialBlockProps } from './TestimonialBlock.types'

const TestimonialBlock: FC<TestimonialBlockProps> = ({
  title,
  titleHighlight,
  testimonials,
}: TestimonialBlockProps): ReactElement => {
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    nextArrow: <Styled.ArrowWrapper><Arrow direction='next' /></Styled.ArrowWrapper>,
    // prevArrow: <Arrow direction='prev' />,
  }
  return (
    <Styled.TestimonialBlock>
      <Section paddingLevel={2}>
        <Styled.HeadingWrapper>
          <Heading
            text={title}
            highlightText={titleHighlight}
            appearance='secondary'
            weight={3}
            highlightStyle='block'
            size={4}
          />
        </Styled.HeadingWrapper>
        <Styled.SliderWrapper>
          <Slider {...settings}>
            {
              testimonials.map((testimonial, index) => {
                return (
                  <Testimonial key={index} {...testimonial} />
                )
              })
            }
          </Slider>
        </Styled.SliderWrapper>
      </Section>
    </Styled.TestimonialBlock>
  )
}

export default TestimonialBlock
