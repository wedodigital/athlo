import { TestimonialProps } from './components/Testimonial/Testimonial.types'

export interface TestimonialBlockProps {
  title: string
  titleHighlight: string
  testimonials: TestimonialProps[]
}
