import React, { ReactElement, FC } from 'react'

import RawHtmlWrapper from '@components/RawHtmlWrapper'
import Heading from '@components/Heading'

import * as Styled from './styles/Testimonial.style'

import { TestimonialProps } from './Testimonial.types'

const Testimonial: FC<TestimonialProps> = ({
  author,
  testimonial,
  image,
}: TestimonialProps): ReactElement => {
  return (
    <Styled.Testimonial>
      <Styled.TestimonialContent>
        <Styled.Content>
          <RawHtmlWrapper content={testimonial} />
        </Styled.Content>
        <Heading size={1} weight={3} text={author} />
      </Styled.TestimonialContent>
      <Styled.TestimonialImage>
        <Styled.Image backgroundImage={image.sourceUrl} />
      </Styled.TestimonialImage>
    </Styled.Testimonial>
  )
}

export default Testimonial
