import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledTestimonialProps {
  theme: Theme;
  backgroundImage: string
}
