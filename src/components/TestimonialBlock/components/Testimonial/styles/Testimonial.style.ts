import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledTestimonialProps } from './Testimonial.style.types'

export const Testimonial = styled.div((props: StyledTestimonialProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  flex-direction: column-reverse;
  gap: ${props.theme.spacing.fixed[4]}px;

  ${props.theme.mixins.respondTo.md(css`
    flex-direction: row;
    gap: ${props.theme.spacing.fixed[4]}px;
  `)}
`)

export const TestimonialContent = styled.div((props: StyledTestimonialProps): FlattenSimpleInterpolation => css`
  text-align: center;
  font-size: ${props.theme.typography.paragraph[1].fontSize};
  line-height: ${props.theme.typography.paragraph[1].lineHeight};

  ${props.theme.mixins.respondTo.md(css`
    text-align: left;
    width: 25%;
    flex-shrink: 0;
  `)}
`)

export const Content = styled.div((props: StyledTestimonialProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[4]}px;
`)

export const TestimonialImage = styled.div((props: StyledTestimonialProps): FlattenSimpleInterpolation => css`
  width: 100%;
  height: 0;
  padding-top: 56.25%;
  background: grey;
  position: relative;
  margin-bottom: ${props.theme.spacing.fixed[4]}px;

  &::before {
    content: '';
    position: absolute;
    background: url('assets/images/pattern.png') top left repeat;
    background-size: 40px auto;
    width: 100%;
    height: 100%;
    top: -${props.theme.spacing.fixed[4]}px;
    right: -${props.theme.spacing.fixed[6]}px;
    z-index: 0;
  }

  img {
    display: block;
    position: relative;
    z-index: 1;
  }

  ${props.theme.mixins.respondTo.md(css`
    padding-top: 46.25%;
    width: 75%;
    margin-bottom: 0;
  `)}
`)

export const Image = styled.div((props: StyledTestimonialProps): FlattenSimpleInterpolation => css`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: url('${props.backgroundImage}') center center no-repeat;
  background-size: cover;
`)
