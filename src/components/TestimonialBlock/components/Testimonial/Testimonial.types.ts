export interface TestimonialProps {
  author: string
  testimonial: string
  image: {
    sourceUrl: string
  }
}
