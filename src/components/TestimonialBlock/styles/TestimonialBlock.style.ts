import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledTestimonialBlockProps } from './TestimonialBlock.style.types'

export const TestimonialBlock = styled.div((props: StyledTestimonialBlockProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.white};
  text-align: center;
`)

export const SliderWrapper = styled.div((props: StyledTestimonialBlockProps): FlattenSimpleInterpolation => css`
  position: relative;
  margin-top: ${props.theme.spacing.fixed[8]}px;
  width: 100%;

  .slick-list {
    overflow: visible !important;
  }
`)

export const HeadingWrapper = styled.div((props: StyledTestimonialBlockProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[8]}px;

  ${props.theme.mixins.respondTo.md(css`
    margin-bottom: ${props.theme.spacing.fixed[10] * 1.5}px;
  `)}
`)

export const ArrowWrapper = styled.div((props: StyledTestimonialBlockProps): FlattenSimpleInterpolation => css`
  position: absolute;
  z-index: 10;
  transform: translate(-50%, -50%);
  width: ${props.theme.spacing.fixed[6]}px;
  height: ${props.theme.spacing.fixed[6]}px;
  top: 120px !important;
  right: 0 !important;

  &::before {
    display: none;
  }

  ${props.theme.mixins.respondTo.md(css`
    top: 50% !important;
    right: 0 !important;
  `)}
`)
