import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledTestimonialBlockProps {
  theme: Theme;
}
