import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledOverlayProps {
  theme: Theme
}
