import styled, { css, createGlobalStyle, FlattenSimpleInterpolation } from 'styled-components'
import hexToRgba from 'hex-to-rgba'

import { StyledOverlayProps } from './Overlay.style.types'

export const HideOverflow = createGlobalStyle((): FlattenSimpleInterpolation => css`
  html {
    overflow: hidden;
  }
  body {
    overflow: visible;
  }
`)

export const Overlay = styled.div((props: StyledOverlayProps): FlattenSimpleInterpolation => css`
  background: ${hexToRgba(props.theme.colours.midGrey, 0.8)};
  position: fixed;
  z-index: 1000;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`)
