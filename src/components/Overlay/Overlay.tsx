import React, { FC, ReactElement } from 'react'

import * as Styled from './styles/Overlay.style'

import { OverlayProps } from './Overlay.types'

const Overlay: FC<OverlayProps> = ({
  onClick,
}: OverlayProps): ReactElement => {
  return (
    <>
      <Styled.HideOverflow />
      <Styled.Overlay onClick={() => onClick()} />
    </>
  )
}

export default Overlay
