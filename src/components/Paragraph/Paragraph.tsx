import React, { FC, ReactElement } from 'react'

import { ParagraphProps } from './Paragraph.types'

import * as Styled from './styles/Paragraph.style'

const Paragraph: FC<ParagraphProps> = ({
  children,
  text,
  size = 3,
  weight = 2,
  inverse = false,
  appearance = 'primary',
}: ParagraphProps): ReactElement => {
  return (
    <Styled.Paragraph appearance={appearance} inverse={inverse} size={size} weight={weight}>
      {text || children}
    </Styled.Paragraph>
  )
}

export default Paragraph
