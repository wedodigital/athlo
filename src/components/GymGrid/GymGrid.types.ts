import { ReactNode } from 'react'

export interface GymGridProps {
  /**
   * React children
   * */
  children: ReactNode;
}
