import React, { ReactElement, FC } from 'react'

import Container from '@components/Container'
import Heading from '@components/Heading'

import * as Styled from './styles/GymGrid.style'

import { GymGridProps } from './GymGrid.types'

const GymGrid: FC<GymGridProps> = ({
  partners,
  columns = 4,
  heading,
}: GymGridProps): ReactElement => {
  return (
    <Styled.GymGrid>
      <Container width='mid'>
        <If condition={heading}>
          <Heading text={heading} size={3} appearance='secondary' justify='center' />
        </If>
        <Styled.Grid>
          {partners.map((partner) => {
            return (
              <Styled.GridContent columns={columns}>
                <a href={partner.url}><img src={partner.logo.sourceUrl} alt={partner.name} /></a>
              </Styled.GridContent>
            )
          })}
        </Styled.Grid>
      </Container>
    </Styled.GymGrid>
  )
}

export default GymGrid
