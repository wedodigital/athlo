import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledGymGridProps {
  theme: Theme;
}
