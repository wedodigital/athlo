import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledGymGridProps } from './GymGrid.style.types'

export const GymGrid = styled.div((props: StyledGymGridProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.white};
  padding: ${props.theme.spacing.fixed[8]}px 0;
`)

export const Grid = styled.div((props: StyledGymGridProps): FlattenSimpleInterpolation => css`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`)

export const GridContent = styled.div((props: StyledGymGridProps): FlattenSimpleInterpolation => css`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  aspect-ratio: 1 / 1;
  width: 50%;
  border-right: 1px dashed ${props.theme.colours.lightBlue};
  border-bottom: 1px dashed ${props.theme.colours.lightBlue};
  gap: ${props.theme.spacing.fixed[3]}px;
  padding: ${props.theme.spacing.fixed[3]}px;

  &:nth-child(2n) {
    border-right: none;
  }

  &:last-child {
    border-right: none !important;
  }

  &:nth-child(2n+1):nth-last-child(-n+2),
  &:nth-child(2n+1):nth-last-child(-n+2) ~ * {
    border-bottom: none;
  }

  img {
    max-width: 150px;
    display: block;
    width: 100%;
  }

  a {
    color: ${props.theme.colours.darkBlue};
  }

  ${props.theme.mixins.respondTo.md(css`
    width: calc(100% / ${props.columns});

    &:nth-child(2n) {
      border-right: 1px dashed ${props.theme.colours.lightBlue};
    }

    &:nth-child(${props.columns}n) {
      border-right: none;
    }

    &:nth-child(${props.columns}n+1):nth-last-child(-n+${props.columns}),
    &:nth-child(${props.columns}n+1):nth-last-child(-n+${props.columns}) ~ * {
      border-bottom: none;
    }
  `)}
`)
