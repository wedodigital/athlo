export interface ArrowProps {
  direction: 'next' | 'prev'
  onClick?: () => void
  position?: 'high' | 'middle' 
}
