import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledArrowProps } from './Arrow.style.types'

export const Arrow = styled.div((props: StyledArrowProps): FlattenSimpleInterpolation => css`
  width: ${props.theme.spacing.fixed[4]}px;
  height: ${props.theme.spacing.fixed[4]}px;
  background: ${props.theme.colours.midBlue};
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;

  ${props.theme.mixins.respondTo.md(css`
    width: ${props.theme.spacing.fixed[6]}px;
    height: ${props.theme.spacing.fixed[6]}px;
  `)}

  & > svg {
    width: ${props.theme.spacing.fixed[2]}px;
    height: ${props.theme.spacing.fixed[2]}px;

    * {
      fill: ${props.theme.colours.white};
    }
  }
`)
