import { Theme } from '@themes/athloTheme/athloTheme.types'

import { ArrowProps } from '../Arrow.types'

export interface StyledArrowProps {
  theme: Theme;
  direction: ArrowProps['direction']
  position: ArrowProps['position']
}
