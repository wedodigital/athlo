import React, { ReactElement, FC } from 'react'

import AngleLeft from '@assets/svg/angle-left.svg'
import AngleRight from '@assets/svg/angle-right.svg'

import * as Styled from './styles/Arrow.style'

import { ArrowProps } from './Arrow.types'

const Arrow: FC<ArrowProps> = ({
  direction,
  onClick,
  position = 'middle'
}: ArrowProps): ReactElement => {
  if (onClick === null) return null
  return (
    <Styled.Arrow onClick={onClick} direction={direction} position={position}>
      <Choose>
        <When condition={direction === 'prev'}>
          <AngleLeft />
        </When>
        <When condition={direction === 'next'}>
          <AngleRight />
        </When>
      </Choose>
    </Styled.Arrow>
  )
}

export default Arrow
