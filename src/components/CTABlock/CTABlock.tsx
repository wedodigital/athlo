import React, { ReactElement, FC, useContext } from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import Section from '@components/Section'
import Heading from '@components/Heading'
import Paragraph from '@components/Paragraph'
import Button from '@components/Button'
import AppStoreButtons from '@components/AppStoreButtons'

import * as Styled from './styles/CTABlock.style'

const CTABlock: FC = (): ReactElement => {
  const componentContent = useStaticQuery(graphql`
    query Cta {
      wpPage(databaseId: {eq: 2}) {
        homepageContent {
          appStoreUrl
          googlePlayUrl
          backgroundImage {
            sourceUrl
          }
          ctaBlockSubtitle
          ctaBlockTitle
          ctaBlockTitleHighlight
        }
      }
    }
  `)

  const content = componentContent.wpPage.homepageContent

  return (
    <Styled.CTABlock>
      <Section bottomPadding={false}>
        <Styled.Content>
          <Styled.ContentBlock>
            <Styled.ContentWrapper>
              <Heading
                text={content.ctaBlockTitle}
                highlightText={content.ctaBlockTitleHighlight}
                appearance='secondary'
                inverse
                size={4}
              />
              <Paragraph inverse text={content.ctaBlockSubtitle} />
            </Styled.ContentWrapper>
            <Choose>
              <When condition={content.appStoreUrl && content.googlePlayUrl}>
                <AppStoreButtons
                  justify='left'
                  appStore={content.appStoreUrl}
                  googlePlay={content.googlePlayUrl}
                />
              </When>
              <Otherwise>
                <Button
                  inverse
                  href={content.appStoreUrl}
                  text='Download Now'
                />
              </Otherwise>
            </Choose>
          </Styled.ContentBlock>
          <Styled.ImageWrapper>
            <img src={content.backgroundImage.sourceUrl} alt={`${content.ctaBlockTitle} ${content.ctaBlockTitleHighlight}`} />
          </Styled.ImageWrapper>
        </Styled.Content>
      </Section>
    </Styled.CTABlock>
  )
}

export default CTABlock
