import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledCTABlockProps {
  theme: Theme;
}
