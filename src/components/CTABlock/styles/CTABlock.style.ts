import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledCTABlockProps } from './CTABlock.style.types'

export const CTABlock = styled.div((props: StyledCTABlockProps): FlattenSimpleInterpolation => css`
  color: ${props.theme.colours.white};
  background: ${props.theme.colours.lightBlue};
`)

export const Content = styled.div((props: StyledCTABlockProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
  gap: ${props.theme.spacing.fixed[4]}px;

  ${props.theme.mixins.respondTo.md(css`
    flex-direction: row;
    gap: ${props.theme.spacing.fixed[8]}px;
  `)}
`)

export const ContentBlock = styled.div((props: StyledCTABlockProps): FlattenSimpleInterpolation => css`
  text-align: center;

  @supports not (gap: ${props.theme.spacing.fixed[4]}px) {
    margin: 0 0 ${props.theme.spacing.fixed[4]}px;
  }

  ${props.theme.mixins.respondTo.md(css`
    text-align: left;
    width: 35%;
    flex-shrink: 0;
    padding: ${props.theme.spacing.fixed[8]}px 0;
  `)}
`)

export const ContentWrapper = styled.div((props: StyledCTABlockProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[4]}px;

  ${props.theme.mixins.respondTo.md(css`
    margin-bottom: ${props.theme.spacing.fixed[8]}px;
  `)}
`)

export const ImageWrapper = styled.div((): FlattenSimpleInterpolation => css`
  margin-left: auto;
  align-self: flex-end;

  img {
    display: block;
  }
`)
