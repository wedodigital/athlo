import React, { ReactElement, FC, useState } from 'react'
import { Formik, Field } from 'formik'

import RawHtmlWrapper from '@components/RawHtmlWrapper'
import Heading from '@components/Heading'
import Button from '@components/Button'

import * as Styled from '@components/PreRegistration/styles/PreRegistration.style'

import { GymPartnerProps } from './GymPartner.types'

const GymPartner: FC<GymPartnerProps> = ({
  showTitle = true,
}: GymPartnerProps): ReactElement => {
  const [formCompleted, setFormCompleted] = useState(false)

  return (
    <>
      <Choose>
        <When condition={formCompleted}>
          <Heading size={3} weight={3} text='Thank you for registering' />
        </When>
        <Otherwise>
          <If condition={showTitle}>
            <Heading size={3} weight={3} text='Become a Gym Partner' />
          </If>
          <RawHtmlWrapper content='Register your gym today to become a partner member of Athlo.' />
          <Formik
            initialValues={{
              first_name: '',
              last_name: '',
              gym_name: '',
              address_1: '',
              address_2: '',
              city: '',
              state: '',
              postcode: '',
              mobile: '',
              email: '',
            }}
            onSubmit={(values) => {
              const formData = new FormData()
              formData.append('g', 'Rsa2Ck')
              formData.append('$fields', 'first_name,last_name,phone_number,Address 1,Address 2,Town,City,Postcode,Gym Name')
              formData.append('email', values.email)
              formData.append('phone_number', values.mobile)
              formData.append('first_name', values.first_name)
              formData.append('last_name', values.last_name)
              formData.append('Gym Name', values.gym_name)
              formData.append('Address 1', values.address_1)
              formData.append('Address 2', values.address_2)
              formData.append('Town', values.city)
              formData.append('City', values.state)
              formData.append('Postcode', values.postcode)
              fetch('https://manage.kmail-lists.com/ajax/subscriptions/subscribe', {
                method: 'POST',
                body: formData,
                redirect: 'follow'
              })
                .then(response => response.text())
                .then(() => setFormCompleted(true))
                .catch(error => console.log('error', error))
            }}
          >
            {() => (
              <Styled.Form>
                <Styled.SplitFormSection>
                  <Styled.FormField>
                    <label htmlFor="first_name">First Name</label>
                    <Field id="first_name" name="first_name" placeholder='Your name' />
                  </Styled.FormField>
                  <Styled.FormField>
                    <label htmlFor="last_name">Last Name</label>
                    <Field id="last_name" name="last_name" placeholder='Your surname' />
                  </Styled.FormField>
                </Styled.SplitFormSection>
                <Styled.FormField>
                  <label htmlFor="gym_name">Gym Name</label>
                  <Field id="gym_name" name="gym_name" placeholder='My Gym' />
                </Styled.FormField>
                <Styled.FormField>
                  <label htmlFor="address_1">Address Line 1</label>
                  <Field id="address_1" name="address_1" placeholder='123 Gym Street' />
                </Styled.FormField>
                <Styled.FormField>
                  <label htmlFor="address_2">Address Line 2</label>
                  <Field id="address_2" name="address_2" placeholder='' />
                </Styled.FormField>
                <Styled.SplitFormSection>
                  <Styled.FormField>
                    <label htmlFor="city">Town</label>
                    <Field id="city" name="city" placeholder='' />
                  </Styled.FormField>
                  <Styled.FormField>
                    <label htmlFor="state">City</label>
                    <Field id="state" name="state" placeholder='' />
                  </Styled.FormField>
                  <Styled.FormField>
                    <label htmlFor="postcode">Postcode</label>
                    <Field id="postcode" name="postcode" placeholder='' />
                  </Styled.FormField>
                </Styled.SplitFormSection>
                <Styled.FormField>
                  <label htmlFor="email">Email Address</label>
                  <Field id="email" name="email" placeholder='your_name@email.com' />
                </Styled.FormField>
                <Styled.FormField>
                  <label htmlFor="mobile">Telephone Number</label>
                  <Field id="mobile" name="mobile" placeholder='01234 567 890' />
                </Styled.FormField>
                <Styled.ButtonWrapper>
                  <Button type='submit' text='Become a Partner' appearance='secondary' size={1} />
                </Styled.ButtonWrapper>
              </Styled.Form>
            )}
          </Formik>
        </Otherwise>
      </Choose>
    </>
  )
}

export default GymPartner
