import React, { ReactElement, FC, useState } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { Formik, Field } from 'formik'
import dayjs from 'dayjs'

import RawHtmlWrapper from '@components/RawHtmlWrapper'
import Heading from '@components/Heading'
import Button from '@components/Button'

import AngleDown from '@assets/svg/angle-down.svg'

import * as Styled from './styles/PreRegistration.style'

import { PreRegistrationProps } from './PreRegistration.types'

const PreRegistration: FC<PreRegistrationProps> = ({
  showTitle = true,
}: PreRegistrationProps): ReactElement => {
  const preRegistration = useStaticQuery(graphql`
    query preRegistrationContent {
      wpPage(databaseId: {eq: 2}) {
        preRegistration {
          preRegistrationTitle
          preRegistrationText
          preRegistrationConfirmationTitle
          preRegistrationConfirmationText
        }
      }
    }
  `)

  const [formCompleted, setFormCompleted] = useState(false)

  const selectDays = (): number[] => {
    const days = []
    for (let i = 1; i < 32; i++) {
      days.push(i)
    }
    return days
  }

  const selectMonths = (): number[] => {
    const months = []
    for (let i = 1; i < 13; i++) {
      months.push(i)
    }
    return months
  }

  const selectYears = (): number[] => {
    const years = []
    const currentYear = dayjs().year()
    for (let i = currentYear - 100; i < currentYear - 18; i++) {
      years.push(i)
    }
    return years.reverse()
  }

  return (
    <>
      <Choose>
        <When condition={formCompleted}>
          <Heading size={3} weight={3} text={preRegistration.wpPage.preRegistration.preRegistrationConfirmationTitle} />
          <RawHtmlWrapper content={preRegistration.wpPage.preRegistration.preRegistrationConfirmationText} />
        </When>
        <Otherwise>
          <If condition={showTitle}>
            <Heading size={3} weight={3} text={preRegistration.wpPage.preRegistration.preRegistrationTitle} />
          </If>
          <RawHtmlWrapper content={preRegistration.wpPage.preRegistration.preRegistrationText} />
          <Formik
            initialValues={{
              first_name: '',
              last_name: '',
              date_of_birth: {
                day: (`0${selectDays()[0]}`).slice(-2),
                month: (`0${selectMonths()[0]}`).slice(-2),
                year: selectYears()[0],
              },
              mobile: '',
              email: '',
              current_member: '',
            }}
            onSubmit={(values) => {
              const formData = new FormData()
              formData.append('g', 'TN2ByD')
              formData.append('$fields', 'first_name,last_name,phone_number,Date of Birth,Current Gym Member')
              formData.append('email', values.email)
              formData.append('phone_number', values.mobile)
              formData.append('first_name', values.first_name)
              formData.append('last_name', values.last_name)
              formData.append('Date of Birth', `${values.date_of_birth.month}/${values.date_of_birth.day}/${values.date_of_birth.year}`)
              formData.append('Current Gym Member', values.current_member)
              fetch('https://manage.kmail-lists.com/ajax/subscriptions/subscribe', {
                method: 'POST',
                body: formData,
                redirect: 'follow'
              })
                .then(response => response.text())
                .then(() => setFormCompleted(true))
                .catch(error => console.log('error', error))
            }}
          >
            {() => (
              <Styled.Form>
                <Styled.SplitFormSection>
                  <Styled.FormField>
                    <label htmlFor="first_name">First Name</label>
                    <Field id="first_name" name="first_name" placeholder='Your name' />
                  </Styled.FormField>
                  <Styled.FormField>
                    <label htmlFor="last_name">Last Name</label>
                    <Field id="last_name" name="last_name" placeholder='Your surname' />
                  </Styled.FormField>
                </Styled.SplitFormSection>
                <Heading text='Date of Birth' weight={3} size={1} />
                <Styled.SplitFormSection>
                  <Styled.FormField>
                    <label htmlFor="date_of_birth.day">Date</label>
                    <Styled.SelectWrapper>
                      <Styled.SelectIcon>
                        <AngleDown />
                      </Styled.SelectIcon>
                      <Field id="date_of_birth.day" name="date_of_birth.day" as='select'>
                        {
                          selectDays().map((day) => {
                            return (
                              <option value={(`0${day}`).slice(-2)} key={day}>{(`0${day}`).slice(-2)}</option>
                            )
                          })
                        }
                      </Field>
                    </Styled.SelectWrapper>
                  </Styled.FormField>
                  <Styled.FormField>
                    <label htmlFor="date_of_birth.month">Month</label>
                    <Styled.SelectWrapper>
                      <Styled.SelectIcon>
                        <AngleDown />
                      </Styled.SelectIcon>
                      <Field id="date_of_birth.month" name="date_of_birth.month" as='select'>
                        {
                          selectMonths().map((month) => {
                            return (
                              <option value={(`0${month}`).slice(-2)} key={month}>{(`0${month}`).slice(-2)}</option>
                            )
                          })
                        }
                      </Field>
                    </Styled.SelectWrapper>
                  </Styled.FormField>
                  <Styled.FormField>
                    <label htmlFor="date_of_birth.year">Year</label>
                    <Styled.SelectWrapper>
                      <Styled.SelectIcon>
                        <AngleDown />
                      </Styled.SelectIcon>
                      <Field id="date_of_birth.year" name="date_of_birth.year" as='select'>
                        {
                          selectYears().map((year) => {
                            return (
                              <option value={year} key={year}>{year}</option>
                            )
                          })
                        }
                      </Field>
                    </Styled.SelectWrapper>
                  </Styled.FormField>
                </Styled.SplitFormSection>
                <Styled.FormField>
                  <label htmlFor="email">Email Address</label>
                  <Field id="email" name="email" placeholder='your_name@email.com' />
                </Styled.FormField>
                <Styled.FormField>
                  <label htmlFor="mobile">Telephone Number</label>
                  <Field id="mobile" name="mobile" placeholder='01234 567 890' />
                </Styled.FormField>
                <Styled.FormField>
                  <label htmlFor="current_member">Are you currently a member at a gym?</label>
                  <Styled.SplitFormSection>
                    <Styled.RadioWrapper>
                      <Styled.Radio type="radio" name="current_member" value="Yes" />
                      <span className='checkmark'></span>
                      <Styled.LabelText>Yes</Styled.LabelText>
                    </Styled.RadioWrapper>
                    <Styled.RadioWrapper>
                      <Styled.Radio type="radio" name="current_member" value="No" />
                      <span className='checkmark'></span>
                      <Styled.LabelText>No</Styled.LabelText>
                    </Styled.RadioWrapper>
                  </Styled.SplitFormSection>
                </Styled.FormField>
                <Styled.ButtonWrapper>
                  <Button type='submit' text='Pre-Register Now' appearance='secondary' size={1} />
                </Styled.ButtonWrapper>
              </Styled.Form>
            )}
          </Formik>
        </Otherwise>
      </Choose>
    </>
  )
}

export default PreRegistration
