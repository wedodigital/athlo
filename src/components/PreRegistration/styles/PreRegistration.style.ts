import styled, { css, FlattenSimpleInterpolation } from 'styled-components'
import { Form as FormikForm, Field } from 'formik'

import { StyledPreRegistrationProps } from './PreRegistration.style.types'

export const FormField = styled.div((props: StyledPreRegistrationProps): FlattenSimpleInterpolation => css`
  flex-grow: 1;
  margin-bottom: ${props.theme.spacing.fixed[2]}px;

  label {
    font-weight: 600;
    display: block;
    margin-bottom: ${props.theme.spacing.fixed[1]}px;
    font-size: ${props.theme.typography.heading[1].fontSize};
    line-height: ${props.theme.typography.heading[1].lineHeight}; 
  }

  input, select {
    width: 100%;
    padding: ${props.theme.spacing.fixed[1]}px ${props.theme.spacing.fixed[2]}px;
    border: 1px solid ${props.theme.colours.midGrey};
    border-radius: 4px;
    font-family: ${props.theme.typography.fontFamily};
    font-size: ${props.theme.typography.paragraph[2].fontSize};
    line-height: ${props.theme.typography.paragraph[2].lineHeight};
    background: ${props.theme.colours.white};
  }

  select {
    appearance: none;

    &::-ms-expand {
      display: none;
    }
  }
`)

export const SplitFormSection = styled.div((props: StyledPreRegistrationProps): FlattenSimpleInterpolation => css`
  display: flex;
  flex-wrap: wrap;
  gap: ${props.theme.spacing.fixed[2]}px;
  margin-bottom: ${props.theme.spacing.fixed[2]}px;

  ${props.theme.mixins.respondTo.md(css`
    flex-wrap: nowrap;
  `)}

  ${FormField} {
    margin-bottom: 0;
  }
`)

export const Form = styled(FormikForm)((props: StyledPreRegistrationProps): FlattenSimpleInterpolation => css`
  margin-top: ${props.theme.spacing.fixed[4]}px;
  padding: ${props.theme.spacing.fixed[4]}px 0;
  border-top: 1px solid ${props.theme.colours.midBlue};
  border-bottom: 1px solid ${props.theme.colours.midBlue};
`)

export const SelectWrapper = styled.div((): FlattenSimpleInterpolation => css`
  position: relative;
`)

export const SelectIcon = styled.span((props: StyledPreRegistrationProps): FlattenSimpleInterpolation => css`
  position: absolute;
  top: 50%;
  right: ${props.theme.spacing.fixed[2]}px;
  transform: translateY(-50%);
  width: ${props.theme.spacing.fixed[2]}px;
  height: ${props.theme.spacing.fixed[2]}px;
  pointer-events: none;

  & > svg {
    width: 100%;
    height: 100%;

    * {
      fill: ${props.theme.colours.midBlue};
    }
  }
`)

export const ButtonWrapper = styled.div((props: StyledPreRegistrationProps): FlattenSimpleInterpolation => css`
  display: flex;
  justify-content: flex-end;
  padding-top: ${props.theme.spacing.fixed[2]}px;
`)

export const RadioWrapper = styled.label((props: StyledPreRegistrationProps): FlattenSimpleInterpolation => css`
  display: block;
  position: relative;
  padding-left: ${props.theme.spacing.fixed[6]}px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  user-select: none;

  .checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: ${props.theme.spacing.fixed[4]}px;
    width: ${props.theme.spacing.fixed[4]}px;
    background: ${props.theme.colours.white};
    border-radius: 50%;
    border: 2px solid ${props.theme.colours.black};

    &::after {
      content: "";
      position: absolute;
      display: block;
      height: ${props.theme.spacing.fixed[2]}px;
      width: ${props.theme.spacing.fixed[2]}px;
      background: ${props.theme.colours.white};
      top: calc(${props.theme.spacing.fixed[1]}px - 2px);
      left: calc(${props.theme.spacing.fixed[1]}px - 2px);
      border-radius: 50%;
    }
  }

  input:checked ~ .checkmark {
    &::after {
      background: ${props.theme.colours.midBlue};
    }
  }
`)

export const Radio = styled(Field)((): FlattenSimpleInterpolation => css`
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
`)

export const LabelText = styled.div((): FlattenSimpleInterpolation => css`
  padding-top: 3px;
`)
