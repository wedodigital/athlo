import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledPreRegistrationProps {
  theme: Theme;
}
