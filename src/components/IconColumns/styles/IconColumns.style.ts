import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledIconColumnsProps } from './IconColumns.style.types'

type Theme = Pick<StyledIconColumnsProps, 'theme'>

export const IconColumns = styled.div((props: Theme): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.lightGrey};
`)

export const Columns = styled.div((props: Theme): FlattenSimpleInterpolation => css`
  display: flex;
  gap: ${props.theme.spacing.fixed[8]}px;
  flex-direction: column;

  ${props.theme.mixins.respondTo.md(css`
    flex-direction: row;
  `)}
`)

export const Column = styled.div((): FlattenSimpleInterpolation => css`
  flex-grow: 1;
  text-align: center;
  width: 100%;
`)

export const Icon = styled.div((props: StyledIconColumnsProps): FlattenSimpleInterpolation => css`
  border: 2px solid ${props.theme.colours.midBlue};
  width: ${props.theme.spacing.fixed[6]}px;
  height: ${props.theme.spacing.fixed[6]}px;
  margin: 0 auto ${props.theme.spacing.fixed[4]}px;
  border-radius: ${props.theme.spacing.fixed[1]}px;

  ${props.backgroundImage && css`
    background: url('${props.backgroundImage}') center center no-repeat;
    background-size: ${props.theme.spacing.fixed[4]}px auto;
  `}
`)
