import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledIconColumnsProps {
  theme: Theme;
  backgroundImage: string
}
