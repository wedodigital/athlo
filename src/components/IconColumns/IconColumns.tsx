import React, { ReactElement, FC } from 'react'

import Section from '@components/Section'
import Heading from '@components/Heading'
import Paragraph from '@components/Paragraph'
import MatchMedia from '@components/MatchMedia'

import * as Styled from './styles/IconColumns.style'

import { IconColumnsProps } from './IconColumns.types'

const IconColumns: FC<IconColumnsProps> = ({
  iconColumns,
}: IconColumnsProps): ReactElement => {
  return (
    <Styled.IconColumns>
      <MatchMedia breakpoint='sm'>
        <Section paddingLevel={2}>
          <Styled.Columns>
            {iconColumns.map((iconColumn, index) => {
              return (
                <Styled.Column key={index}>
                  <Styled.Icon backgroundImage={iconColumn.icon.sourceUrl} />
                  <Heading size={1} weight={3} text={iconColumn.title} />
                  <Paragraph size={2} text={iconColumn.content} />
                </Styled.Column>
              )
            })}
          </Styled.Columns>
        </Section>
      </MatchMedia>
      <MatchMedia breakpoint='md' andAbove>
        <Section>
          <Styled.Columns>
            {iconColumns.map((iconColumn, index) => {
              return (
                <Styled.Column key={index}>
                  <Styled.Icon backgroundImage={iconColumn.icon.sourceUrl} />
                  <Heading size={1} weight={3} text={iconColumn.title} />
                  <Paragraph size={2} text={iconColumn.content} />
                </Styled.Column>
              )
            })}
          </Styled.Columns>
        </Section>
      </MatchMedia>
    </Styled.IconColumns>
  )
}

export default IconColumns
