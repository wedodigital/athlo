export interface IconColumnsProps {
  iconColumns: {
    backgroundImage?: {
      sourceUrl: string
    }
    title: string
    content: string
  }[]
}
