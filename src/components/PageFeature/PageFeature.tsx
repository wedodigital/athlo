import React, { ReactElement, FC } from 'react'

import Heading from '@components/Heading'
import RawHtmlWrapper from '@components/RawHtmlWrapper'
import Button from '@components/Button'
import Container from '@components/Container'

import * as Styled from './styles/PageFeature.style'

import { PageFeatureProps } from './PageFeature.types'

const PageFeature: FC<PageFeatureProps> = ({
  backgroundImage,
  title,
  introText,
}: PageFeatureProps): ReactElement => {
  return (
    <Styled.PageFeature backgroundImage={backgroundImage}>
      <Container>
        <Styled.Content>
          <Heading
            text={title}
            size={5}
            level={1}
            inverse
            highlightStyle='block'
            noMargin
          />
          <If condition={introText}>
            <RawHtmlWrapper content={introText} inverse />
          </If>
          <If condition={backgroundImage}>
            <Button href='mailto:support@athlo.app' text='support@athlo.app' inverse />
          </If>
        </Styled.Content>
      </Container>
    </Styled.PageFeature>
  )
}

export default PageFeature
