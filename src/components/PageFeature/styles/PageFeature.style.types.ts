import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledPageFeatureProps {
  theme: Theme
  backgroundImage: string
}
