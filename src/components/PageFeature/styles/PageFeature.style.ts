import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledPageFeatureProps } from './PageFeature.style.types'

export const PageFeature = styled.div((props: StyledPageFeatureProps): FlattenSimpleInterpolation => css`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 400px;
  background: linear-gradient(to bottom, ${props.theme.colours.midBlue} 0%, ${props.theme.colours.darkBlue} 100%);

  ${props.backgroundImage && css`
    height: 100vh;
    background: url('${props.backgroundImage}') center center no-repeat;
    background-size: cover;
  `}
`)

export const Content = styled.div((props: StyledPageFeatureProps): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  text-align: center;
  gap: ${props.theme.spacing.fixed[6]}px;
  max-width: 400px;
  margin: 0 auto;
`)
