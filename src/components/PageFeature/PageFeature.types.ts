export interface PageFeatureProps {
  backgroundImage?: string
  title: string
  introText?: string
}
