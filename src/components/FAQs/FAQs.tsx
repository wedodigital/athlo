import React, { ReactElement, FC } from 'react'

import Container from '@components/Container'

import Question from './components/Question'

import * as Styled from './styles/FAQs.style'

import { FAQsProps } from './FAQs.types'

const FAQs: FC<FAQsProps> = ({
  faqs,
}: FAQsProps): ReactElement => {
  return (
    <Styled.FAQs>
      <Container width='mid'>
        {
          faqs.map((question, index) => {
            return (
              <Question
                key={index}
                question={question.question}
                answer={question.answer}
              />
            )
          })
        }
      </Container>
    </Styled.FAQs>
  )
}

export default FAQs
