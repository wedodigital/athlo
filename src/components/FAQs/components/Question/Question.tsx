import React, { ReactElement, FC, useState } from 'react'
import AnimateHeight from 'react-animate-height'

import Increase from '@assets/svg/increase.svg'

import Heading from '@components/Heading'
import RawHtmlWrapper from '@components/RawHtmlWrapper'

import * as Styled from './styles/Question.style'

import { QuestionProps } from './Question.types'

const Question: FC<QuestionProps> = ({
  question,
  answer,
}: QuestionProps): ReactElement => {
  const [isOpen, setIsOpen] = useState(false)

  return (
    <Styled.Question>
      <Styled.Title>
        <Heading size={1} weight={2} noMargin text={question} />
        <Styled.Toggle isOpen={isOpen} onClick={() => setIsOpen(!isOpen)}>
          <Increase />
        </Styled.Toggle>
      </Styled.Title>
      <AnimateHeight
        duration={ 500 }
        height={isOpen ? 'auto' : 0}
      >
        <Styled.Content>
          <RawHtmlWrapper content={answer} />
        </Styled.Content>
      </AnimateHeight>
    </Styled.Question>
  )
}

export default Question
