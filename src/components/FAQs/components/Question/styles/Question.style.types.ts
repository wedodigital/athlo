import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledQuestionProps {
  theme: Theme
  isOpen: boolean
}
