import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledQuestionProps } from './Question.style.types'

export const Question = styled.div((props: StyledQuestionProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[2]}px;
  border-bottom: 1px solid #ccc;

  &:first-child {
    border-top: 1px solid #ccc;
  }
`)

export const Title = styled.div((): FlattenSimpleInterpolation => css`
  display: flex;
  align-items: center;
  justify-content: space-between;
`)

export const Content = styled.div((props: StyledQuestionProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[2]}px 0;
`)

export const Toggle = styled.button((props: StyledQuestionProps): FlattenSimpleInterpolation => css`
  background: none;
  color: ${props.theme.colours.midBlue};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  text-decoration: none;
  font-weight: 700;
  border: none;
  cursor: pointer;
  font-family: ${props.theme.typography.fontFamily};
  width: ${props.theme.spacing.fixed[4]}px;
  height: ${props.theme.spacing.fixed[4]}px;
  transition: 0.4s all ease;
  flex-wrap: wrap;

  ${props.isOpen && css`
    transform: rotate(225deg);
  `}

  & > svg {
    width: ${props.theme.spacing.fixed[3]}px;
    height: ${props.theme.spacing.fixed[3]}px;

    * {
      fill: ${props.theme.colours.midBlue};
    }
  }

  & > span {
    display: block;
  }
`)
