import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledFAQsProps {
  theme: Theme;
}
