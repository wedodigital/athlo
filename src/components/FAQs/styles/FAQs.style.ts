import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledFAQsProps } from './FAQs.style.types'

export const FAQs = styled.div((props: StyledFAQsProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.white};
  padding: ${props.theme.spacing.fixed[8]}px 0;
`)
