export interface FAQsProps {
  faqs: {
    question: string
    answer: string
  }[]
}
