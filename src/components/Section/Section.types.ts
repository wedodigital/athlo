import { ReactNode } from 'react'
import { ContainerProps } from '@components/Container/Container.types'

export interface SectionProps {
  /**
   * React children
   * */
  children: ReactNode
  titleSection?: boolean
  paddingLevel?: 1 | 2
  bottomPadding?: boolean
  containerWidth?: ContainerProps['width']
}
