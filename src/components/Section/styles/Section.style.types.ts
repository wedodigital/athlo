import { Theme } from '@themes/athloTheme/athloTheme.types'

import { SectionProps } from '../Section.types'

export interface StyledSectionProps {
  theme: Theme
  titleSection: boolean
  paddingLevel: SectionProps['paddingLevel']
  bottomPadding: boolean
}
