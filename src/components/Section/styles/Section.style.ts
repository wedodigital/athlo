import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledSectionProps } from './Section.style.types'

export const Section = styled.div((props: StyledSectionProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[4]}px 0;
  overflow: hidden;

  ${props.paddingLevel === 2 && css`
    padding: ${props.theme.spacing.fixed[8]}px 0;
  `}

  ${props.theme.mixins.respondTo.md(css`
    padding: ${props.theme.spacing.fixed[8]}px 0;

    ${props.paddingLevel === 2 && css`
      padding: ${props.theme.spacing.fixed[10] * 1.5}px 0;
    `}
  `)}

  ${!props.bottomPadding && css`
    padding-bottom: 0 !important;
  `}

  ${props.titleSection && css`
    height: 50vh;
    display: flex;
    align-items: flex-end;
  `}
`)
