import React, { ReactElement, FC } from 'react'

import Container from '@components/Container'

import * as Styled from './styles/Section.style'

import { SectionProps } from './Section.types'

const Section: FC<SectionProps> = ({
  children,
  titleSection,
  paddingLevel = 1,
  bottomPadding = true,
  containerWidth,
}: SectionProps): ReactElement => {
  return (
    <Styled.Section titleSection={titleSection} bottomPadding={bottomPadding} paddingLevel={paddingLevel}>
      <Container width={containerWidth}>
        {children}
      </Container>
    </Styled.Section>
  )
}

export default Section
