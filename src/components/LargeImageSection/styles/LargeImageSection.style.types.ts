import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledLargeImageSectionProps {
  theme: Theme
  backgroundImage: string
}
