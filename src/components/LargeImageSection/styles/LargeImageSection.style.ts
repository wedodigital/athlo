import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledLargeImageSectionProps } from './LargeImageSection.style.types'

export const LargeImageSection = styled.div((props: StyledLargeImageSectionProps): FlattenSimpleInterpolation => css`
  text-align: center;
  min-height: 500px;
  display: flex;
  align-items: center;
  justify-content: center;
  background: url('${props.backgroundImage}') center center no-repeat ${props.theme.colours.black};
  background-size: cover;

  & > * {
    width: 100%;
  }
`)
