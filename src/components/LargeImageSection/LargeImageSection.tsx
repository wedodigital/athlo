import React, { ReactElement, FC } from 'react'

import Section from '@components/Section'
import Heading from '@components/Heading'
import Paragraph from '@components/Paragraph'
import Logo from '@components/Logo'

import * as Styled from './styles/LargeImageSection.style'

import { LargeImageSectionProps } from './LargeImageSection.types'

const LargeImageSection: FC<LargeImageSectionProps> = ({
  title,
  backgroundImage,
  subTitle,
}: LargeImageSectionProps): ReactElement => {
  return (
    <Styled.LargeImageSection backgroundImage={backgroundImage}>
      <Section>
        <Logo />
        <Heading text={title} size={4} weight={3} inverse />
        <Paragraph text={subTitle} inverse />
      </Section>
    </Styled.LargeImageSection>
  )
}

export default LargeImageSection
