export interface LargeImageSectionProps {
  title: string
  backgroundImage: string
  subTitle: string
}
