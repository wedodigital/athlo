import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledRawHtmlWrapperProps {
  theme: Theme
  inverse: boolean
}
