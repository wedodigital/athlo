import React, { FC, ReactElement } from 'react'

import { RawHtmlWrapperProps } from './RawHtmlWrapper.types'

import * as Styled from './styles/RawHtmlWrapper.style'

const RawHtmlWrapper: FC<RawHtmlWrapperProps> = ({
  content,
  inverse,
}: RawHtmlWrapperProps): ReactElement => {
  return <Styled.RawHtmlWrapper inverse={inverse} dangerouslySetInnerHTML={{ __html: content }} />
}

export default RawHtmlWrapper
