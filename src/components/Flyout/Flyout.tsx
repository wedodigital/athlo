import React, { ReactElement, FC, useContext } from 'react'

import Overlay from '@components/Overlay'
import Logo from '@components/Logo'
import FlyoutNavigation from '@components/FlyoutNavigation'

import Close from '@assets/svg/close.svg'

import ModalContext from '@context/ModalContext'
import { ModalContextProps } from '@context/ModalContext.types'

import * as Styled from './styles/Flyout.style'

import { FlyoutProps } from './Flyout.types'

const Flyout: FC<FlyoutProps> = ({
  isOpen = false,
}: FlyoutProps): ReactElement => {
  const modalContext = useContext(ModalContext) as ModalContextProps
  return (
    <>
      <If condition={isOpen}>
        <Overlay onClick={() => modalContext.toggleFlyout()} />
      </If>
      <Styled.Flyout isOpen={isOpen}>
        <Styled.FlyoutContent>
          <Styled.LogoWrapper>
            <Logo logoStyle='dark' />
            <Styled.CloseButton onClick={() => modalContext.toggleFlyout()}>
              <Close />
            </Styled.CloseButton>
          </Styled.LogoWrapper>
          <FlyoutNavigation />
        </Styled.FlyoutContent>
      </Styled.Flyout>
    </>
  )
}

export default Flyout
