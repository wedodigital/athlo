import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledFlyoutProps } from './Flyout.style.types'

export const Flyout = styled.div((props: StyledFlyoutProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.white};
  position: fixed;
  top: 0;
  bottom: 0;
  left: -100%;
  width: 95%;
  z-index: 1000;
  overflow: auto;
  transition: 0.4s all ease;

  ${props.isOpen && css`
    left: 0;
  `}
`)

export const FlyoutContent = styled.div((props: StyledFlyoutProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[4]}px;
`)

export const LogoWrapper = styled.div((props: StyledFlyoutProps): FlattenSimpleInterpolation => css`
  padding-bottom: ${props.theme.spacing.fixed[4]}px;
  margin-bottom: ${props.theme.spacing.fixed[4]}px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`)

export const CloseButton = styled.button((props: StyledFlyoutProps): FlattenSimpleInterpolation => css`
  background: none;
  color: ${props.theme.colours.midBlue};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  text-decoration: none;
  font-weight: 700;
  border: none;
  cursor: pointer;
  font-family: ${props.theme.typography.fontFamily};
  padding: 0;

  & > svg {
    width: ${props.theme.spacing.fixed[3]}px;
    height: ${props.theme.spacing.fixed[3]}px;

    * {
      fill: ${props.theme.colours.midBlue};
    }
  }
`)
