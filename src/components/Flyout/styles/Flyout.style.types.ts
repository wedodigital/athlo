import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledFlyoutProps {
  theme: Theme
  isOpen: boolean
}
