import { MouseEvent } from 'react'

export type ButtonEvent =
  | MouseEvent<HTMLButtonElement, MouseEvent>
  | MouseEvent<HTMLAnchorElement, MouseEvent>

export interface ButtonProps {
  to?: string
  onInteraction?: (event: ButtonEvent) => void
  href?: string
  appearance?: 'primary' | 'secondary'
  text: string
  inverse?: boolean
  type?: 'submit' | 'button' | 'reset'
  size?: 1 | 2
}
