import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledButtonProps } from './Button.style.types'

export const Button = styled.div((props: StyledButtonProps): FlattenSimpleInterpolation => css`
  cursor: pointer;
  text-decoration: none;
  border-radius: ${props.theme.spacing.fixed[2]}px;
  transition: .4s all ease;
  font-size: ${props.theme.typography.paragraph[3].fontSize};
  line-height: ${props.theme.typography.paragraph[3].lineHeight};
  font-weight: 600;

  ${props.size === 1 && css`
    padding: ${props.theme.spacing.fixed[1]}px ${props.theme.spacing.fixed[3]}px;
  `}

  ${props.size === 2 && css`
    padding: ${props.theme.spacing.fixed[1] * 1.5}px ${props.theme.spacing.fixed[5]}px;
  `}

  ${props.appearance === 'primary' && css`
    color: ${props.theme.colours.white};
    background: ${props.theme.colours.midBlue};
    border: 2px solid ${props.theme.colours.midBlue};

    ${props.inverse && css`
      color: ${props.theme.colours.black};
      background: ${props.theme.colours.white};
      border: 2px solid ${props.theme.colours.white};
    `}
  `}

  ${props.appearance === 'secondary' && css`
    color: ${props.theme.colours.midBlue};
    background: none;
    border: 2px solid ${props.theme.colours.midBlue};

    ${props.inverse && css`
      color: ${props.theme.colours.white};
      background: none;
      border: 2px solid ${props.theme.colours.white};
    `}
  `}

  &:hover {
    opacity: .8;
  }
`)
