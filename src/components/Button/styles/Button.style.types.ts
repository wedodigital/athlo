import { Theme } from '@themes/athloTheme/athloTheme.types'

import { ButtonProps } from '../Button.types'

export interface StyledButtonProps {
  theme: Theme
  appearance: ButtonProps['appearance']
  size: ButtonProps['size']
  inverse: boolean
}
