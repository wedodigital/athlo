import { Theme } from '@themes/athloTheme/athloTheme.types'

import { LogoProps } from '../Logo.types'

export interface StyledLogoProps {
  theme: Theme;
  size: LogoProps['size']
}
