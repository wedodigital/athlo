import styled, { css, FlattenSimpleInterpolation } from 'styled-components'
import { Link } from 'gatsby'

import { StyledLogoProps } from './Logo.style.types'

export const Logo = styled(Link)((props: StyledLogoProps): FlattenSimpleInterpolation => css`
  display: inline-block;
  height: ${props.theme.spacing.fixed[6]}px;

  ${props.size === 1 && css`
    max-width: 140px;
  `}

  ${props.size === 2 && css`
    max-width: 160px;
  `}

  svg {
    height: 100%;
    width: 100%;
  }
`)
