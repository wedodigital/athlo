export interface LogoProps {
  logoStyle: 'light' | 'dark'
  size?: 1 | 2
}
