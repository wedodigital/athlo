import React, { FC, ReactElement } from 'react'

import LogoLight from '@assets/svg/logo-light.svg'
import LogoDark from '@assets/svg/logo-dark.svg'

import { LogoProps } from './Logo.types'

import * as Styled from './styles/Logo.style'

const Logo: FC<LogoProps> = ({ logoStyle = 'light', size = 2 }: LogoProps): ReactElement => {
  return (
    <Styled.Logo to='/' size={size}>
      <Choose>
        <When condition={logoStyle === 'light'}>
          <LogoLight />
        </When>
        <When condition={logoStyle === 'dark'}>
          <LogoDark />
        </When>
      </Choose>
    </Styled.Logo>
  )
}

export default Logo
