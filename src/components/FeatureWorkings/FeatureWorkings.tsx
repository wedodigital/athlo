import React, { ReactElement, FC } from 'react'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import Section from '@components/Section'
import Heading from '@components/Heading'
import Paragraph from '@components/Paragraph'
import Arrow from '@components/Arrow'
import MatchMedia from '@components/MatchMedia'

import FeatureDetails from './components/FeatureDetails'

import * as Styled from './styles/FeatureWorkings.style'

import { FeatureWorkingsProps } from './FeatureWorkings.types'

const FeatureWorkings: FC<FeatureWorkingsProps> = ({
  title,
  titleHighlight,
  subTitle,
  features,
}: FeatureWorkingsProps): ReactElement => {
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    nextArrow: <Styled.ArrowWrapper direction='next'><Arrow direction='next' /></Styled.ArrowWrapper>,
    prevArrow: <Styled.ArrowWrapper direction='prev'><Arrow direction='prev' /></Styled.ArrowWrapper>,
  }
  return (
    <Styled.FeatureWorkings>
      <Section paddingLevel={2}>
        <Styled.HeadingWrapper>
          <Heading size={4} text={title} highlightText={titleHighlight} />
          <Paragraph text={subTitle} />
        </Styled.HeadingWrapper>
        <MatchMedia breakpoint='sm'>
          <Styled.SliderWrapper>
            <Slider {...settings}>
              {
                features.map((featureDetails, index) => {
                  return (
                    <FeatureDetails key={index} {...featureDetails} />
                  )
                })
              }
            </Slider>
          </Styled.SliderWrapper>
        </MatchMedia>
        <MatchMedia breakpoint='md' andAbove>
          <Styled.DetailsBlock>
            {
              features.map((featureDetails, index) => {
                return (
                  <FeatureDetails key={index} {...featureDetails} />
                )
              })
            }
          </Styled.DetailsBlock>
        </MatchMedia>
      </Section>
    </Styled.FeatureWorkings>
  )
}

export default FeatureWorkings
