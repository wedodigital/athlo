import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledFeatureDetailsProps } from './FeatureDetails.style.types'

export const FeatureDetails = styled.div((props: StyledFeatureDetailsProps): FlattenSimpleInterpolation => css`
  flex-grow: 1;
  width: 100%;

  @supports not (gap: ${props.theme.spacing.fixed[8]}px) {
    margin-right: ${props.theme.spacing.fixed[8]}px;

    &:last-child {
      margin-right: 0;
    }
  }
`)

export const ImageBlock = styled.div((props: StyledFeatureDetailsProps): FlattenSimpleInterpolation => css`
  position: relative;
  margin: 0 auto ${props.theme.spacing.fixed[4]}px;
  padding-top: ${props.theme.spacing.fixed[4]}px;
  max-width: 160px;

  ${props.theme.mixins.respondTo.md(css`
    padding-top: 0;
    max-width: 240px;
  `)}


  &::before {
    content: '';
    position: absolute;
    background: url('assets/images/pattern-reverse.png') top left repeat;
    background-size: 26px auto;
    width: 100%;
    height: 100%;
    top: -${props.theme.spacing.fixed[4]}px;
    left: -${props.theme.spacing.fixed[6]}px;
  }

  img {
    display: block;
    position: relative;
    z-index: 1;
  }
`)
