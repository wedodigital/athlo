import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledFeatureDetailsProps {
  theme: Theme;
}
