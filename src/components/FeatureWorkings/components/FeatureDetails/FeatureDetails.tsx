import React, { ReactElement, FC } from 'react'

import Heading from '@components/Heading'
import Paragraph from '@components/Paragraph'

import * as Styled from './styles/FeatureDetails.style'

import { FeatureDetailsProps } from './FeatureDetails.types'

const FeatureDetails: FC<FeatureDetailsProps> = ({
  title,
  content,
  image,
}: FeatureDetailsProps): ReactElement => {
  return (
    <Styled.FeatureDetails>
      <Styled.ImageBlock>
        <img src={image.localFile.childImageSharp.fluid.src} alt={title} />
      </Styled.ImageBlock>
      <Heading size={1} weight={3} text={title} />
      <Paragraph size={2} text={content} />
    </Styled.FeatureDetails>
  )
}

export default FeatureDetails
