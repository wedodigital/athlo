export interface FeatureDetailsProps {
  image: {
    sourceUrl: string
  }
  title: string
  content: string
}
