import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledFeatureWorkingsProps {
  theme: Theme;
  direction: 'prev' | 'next'
}
