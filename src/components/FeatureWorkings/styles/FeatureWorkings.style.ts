import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledFeatureWorkingsProps } from './FeatureWorkings.style.types'

export const FeatureWorkings = styled.div((props: StyledFeatureWorkingsProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.white};
  text-align: center;
`)

export const DetailsBlock = styled.div((props: StyledFeatureWorkingsProps): FlattenSimpleInterpolation => css`
  display: flex;
  padding-top: ${props.theme.spacing.fixed[8]}px;
  gap: ${props.theme.spacing.fixed[8]}px;
`)

export const SliderWrapper = styled.div((): FlattenSimpleInterpolation => css`
  position: relative;
`)

export const HeadingWrapper = styled.div((props: StyledFeatureWorkingsProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[8]}px;

  ${props.theme.mixins.respondTo.md(css`
    margin-bottom: ${props.theme.spacing.fixed[10] * 1.5}px;
  `)}
`)

export const ArrowWrapper = styled.div((props: StyledFeatureWorkingsProps): FlattenSimpleInterpolation => css`
  position: absolute;
  z-index: 10;
  width: ${props.theme.spacing.fixed[4]}px !important;
  height: ${props.theme.spacing.fixed[4]}px !important;
  top: 28% !important;
  transform: none !important;

  &::before {
    display: none;
  }

  ${props.direction === 'prev' && css`
    left: 0 !important;
  `}

  ${props.direction === 'next' && css`
    right: 0 !important;
  `}
`)
