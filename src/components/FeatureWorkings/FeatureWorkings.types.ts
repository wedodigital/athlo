import { FeatureDetailsProps } from './components/FeatureDetails/FeatureDetails.types'

export interface FeatureWorkingsProps {
  title: string
  titleHighlight: string
  subTitle: string
  features: FeatureDetailsProps[]
}
