import React, { ReactElement, FC, useContext } from 'react'
import { Link } from 'gatsby'

import ModalContext from '@context/ModalContext'
import { ModalContextProps } from '@context/ModalContext.types'

import * as Styled from './styles/Navigation.style'

import { NavigationProps } from './Navigation.types'

const Navigation: FC<NavigationProps> = ({
  inverse = false,
}: NavigationProps): ReactElement => {
  const modalContext = useContext(ModalContext) as ModalContextProps
  const navLinks = [
    {
      title: 'Get in Touch',
      url: '/support'
    },
    {
      title: 'Partner Gyms',
      url: '/partners'
    },
    {
      title: 'Become a Gym Partner',
      onClick: modalContext.toggleModal
    }
  ]

  return (
    <Styled.Navigation>
      {
        navLinks.map((navLink, index) => {
          return (
            <li key={index}>
              <Choose>
                <When condition={navLink.onClick}>
                  <Styled.NavLink inverse={inverse} as='button' onClick={() => navLink.onClick('partner')}>
                    {navLink.title}
                  </Styled.NavLink>
                </When>
                <Otherwise>
                  <Styled.NavLink inverse={inverse} as={Link} to={navLink.url}>
                    {navLink.title}
                  </Styled.NavLink>
                </Otherwise>
              </Choose>
            </li>
          )
        })
      }
    </Styled.Navigation>
  )
}

export default Navigation
