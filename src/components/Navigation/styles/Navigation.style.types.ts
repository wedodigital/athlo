import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledNavigationProps {
  theme: Theme
  inverse: boolean
}
