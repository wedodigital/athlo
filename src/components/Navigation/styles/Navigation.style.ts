import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledNavigationProps } from './Navigation.style.types'

export const Navigation = styled.ul((props: StyledNavigationProps): FlattenSimpleInterpolation => css`
  display: flex;
  gap: ${props.theme.spacing.fixed[4]}px;
  font-size: ${props.theme.typography.paragraph[3].fontSize};
  line-height: ${props.theme.typography.paragraph[3].lineHeight};
`)

export const NavLink = styled.div((props: StyledNavigationProps): FlattenSimpleInterpolation => css`
  cursor: pointer;
  text-decoration: none;
  background: none;
  border: none;
  border-bottom: 1px solid transparent;
  transition: .4s all ease;
  font-size: ${props.theme.typography.paragraph[3].fontSize};
  line-height: ${props.theme.typography.paragraph[3].lineHeight};
  font-weight: 600;
  color: ${props.theme.colours.midBlue};

  ${props.inverse && css`
    color: ${props.theme.colours.white};
  `}

  &:hover {
    border-bottom: 1px solid ${props.theme.colours.midBlue};

    ${props.inverse && css`
      border-bottom: 1px solid ${props.theme.colours.white};
    `}
  }
`)
