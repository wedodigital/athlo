import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledFeatureVideoProps } from './FeatureVideo.style.types'

export const FeatureVideo = styled.div((props: StyledFeatureVideoProps): FlattenSimpleInterpolation => css`
  color: ${props.theme.colours.white};
  background: url('${props.backgroundImage}') center center no-repeat;
  background-size: cover;
  height: 100vh;
  position: relative;
`)

export const Content = styled.div((): FlattenSimpleInterpolation => css`
  position: absolute;
  z-index: 20;
  text-align: center;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 100%;
`)

export const ContentWrapper = styled.div((props: StyledFeatureVideoProps): FlattenSimpleInterpolation => css`
  margin-bottom: ${props.theme.spacing.fixed[6]}px;
`)

export const BackgroundVideo = styled.video((): FlattenSimpleInterpolation => css`
  position: absolute;
  top: 50%;
  left: 50%;
  min-width: 100%;
  min-height: 100%;
  width: auto;
  height: auto;
  -webkit-transform: translateX(-50%) translateY(-50%);
  transform: translateX(-50%) translateY(-50%);
  background-size: cover;
`)

export const VideoOverlay = styled.div((): FlattenSimpleInterpolation => css`
  position: absolute;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  z-index: 10;
  background: rgba(0, 0, 0, 0.2);
`)
