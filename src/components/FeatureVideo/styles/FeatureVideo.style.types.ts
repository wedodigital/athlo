import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledFeatureVideoProps {
  theme: Theme;
  backgroundImage: string
}
