export interface FeatureVideoProps {
  title: string
  titleHighlight?: string
  subTitle?: string
  backgroundVideo?: {
    poster: string
    mp4?: string
    webm?: string
  }
  appStore?: string
  googlePlay?: string
}
