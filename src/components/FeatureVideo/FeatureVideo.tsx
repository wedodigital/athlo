import React, { ReactElement, FC, useContext } from 'react'

import Heading from '@components/Heading'
import Container from '@components/Container'
import Paragraph from '@components/Paragraph'
import Button from '@components/Button'
import AppStoreButtons from '@components/AppStoreButtons'

import ModalContext from '@context/ModalContext'
import { ModalContextProps } from '@context/ModalContext.types'

import * as Styled from './styles/FeatureVideo.style'

import { FeatureVideoProps } from './FeatureVideo.types'

const FeatureVideo: FC<FeatureVideoProps> = ({
  title,
  titleHighlight,
  subTitle,
  backgroundVideo,
  appStore,
  googlePlay,
}: FeatureVideoProps): ReactElement => {
  const modalContext = useContext(ModalContext) as ModalContextProps
  return (
    <div>
      <Styled.FeatureVideo backgroundImage={backgroundVideo.poster} />
      <Styled.Content>
        <Container width='mid'>
          <Styled.ContentWrapper>
            <Heading
              text={title}
              highlightText={titleHighlight}
              size={5}
              level={1}
              inverse
              highlightStyle='block'
              justify='center'
            />
            <If condition={subTitle}>
              <Paragraph text={subTitle} size={3} inverse />
            </If>
          </Styled.ContentWrapper>
          <Choose>
            <When condition={appStore && googlePlay}>
              <AppStoreButtons
                appStore={appStore}
                googlePlay={googlePlay}
              />
            </When>
            <Otherwise>
              <Button
                inverse
                href={appStore}
                text='Download Now'
              />
            </Otherwise>
          </Choose>
        </Container>
      </Styled.Content>
      <If condition={backgroundVideo.mp4 || backgroundVideo.webm}>
        <Styled.BackgroundVideo muted autoPlay playsInline loop>
          <source src={backgroundVideo.mp4} type='video/mp4' />
          <source src={backgroundVideo.webm} type='video/webm' />
        </Styled.BackgroundVideo>
      </If>
    </div>
  )
}

export default FeatureVideo
