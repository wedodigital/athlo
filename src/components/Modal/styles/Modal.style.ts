import styled, { css, FlattenSimpleInterpolation } from 'styled-components'

import { StyledModalProps } from './Modal.style.types'

export const ModalWrapper = styled.div((): FlattenSimpleInterpolation => css`
  position: fixed;
  z-index: 1000;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  pointer-events: none;
`)

export const ModalContainer = styled.div((props: StyledModalProps): FlattenSimpleInterpolation => css`
  background: #fff;
  padding: ${props.theme.spacing.fixed[2]}px;
  border-radius: ${props.theme.spacing.fixed[1] / 2}px;
  width: 90%;
  max-width: 700px;
  pointer-events: auto;
  box-shadow: 0 0 10px 0 rgba(0,0,0,0.2);
  position: relative;
  max-height: 80vh;
  overflow: auto;
`)

export const ModalContent = styled.div((props: StyledModalProps): FlattenSimpleInterpolation => css`
  padding: ${props.theme.spacing.fixed[3]}px;
`)

export const CloseButton = styled.button((props: StyledModalProps): FlattenSimpleInterpolation => css`
  background: ${props.theme.colours.midBlue};
  color: ${props.theme.colours.white};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  text-decoration: none;
  font-weight: 700;
  border: none;
  cursor: pointer;
  font-family: ${props.theme.typography.fontFamily};
  width: ${props.theme.spacing.fixed[6]}px;
  height: ${props.theme.spacing.fixed[6]}px;
  position: absolute;
  top: 0;
  right: 0;
  border-radius: 0 4px 0 4px;

  & > svg {
    width: ${props.theme.spacing.fixed[3]}px;
    height: ${props.theme.spacing.fixed[3]}px;

    * {
      fill: ${props.theme.colours.white};
    }
  }
`)
