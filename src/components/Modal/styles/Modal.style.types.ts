import { Theme } from '@themes/athloTheme/athloTheme.types'

export interface StyledModalProps {
  theme: Theme;
  active: boolean
}
