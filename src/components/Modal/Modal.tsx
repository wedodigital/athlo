import React, { FC, ReactElement, useContext } from 'react'
import { CSSTransition } from 'react-transition-group'

import Close from '@assets/svg/close.svg'

import Overlay from '@components/Overlay'

import ModalContext from '@context/ModalContext'
import { ModalContextProps } from '@context/ModalContext.types'

import * as Styled from './styles/Modal.style'

import { ModalProps } from './Modal.types'

const Modal: FC<ModalProps> = ({ children }: ModalProps ): ReactElement => {
  const modalContext = useContext(ModalContext) as ModalContextProps
  return (
    <>
      <Overlay onClick={() => modalContext.toggleModal()} />
      <CSSTransition
        in={modalContext.showModal}
        timeout={500}
        classNames='modal'
        unmountOnExit
      >
        <Styled.ModalWrapper>
          <Styled.ModalContainer>
            <Styled.ModalContent>
              <Styled.CloseButton onClick={() => modalContext.toggleModal()}>
                <Close />
              </Styled.CloseButton>
              {children}
            </Styled.ModalContent>
          </Styled.ModalContainer>
        </Styled.ModalWrapper>
      </CSSTransition>
    </>
  )
}

export default Modal
