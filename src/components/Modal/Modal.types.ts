import { ReactNode } from 'react'

export interface ModalProps {
  children: ReactNode
  width?: 'narrow' | 'default'
}
