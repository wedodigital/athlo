import React from 'react'

import { ModalContextProps } from './ModalContext.types'

export default React.createContext<Partial<ModalContextProps>>({})
