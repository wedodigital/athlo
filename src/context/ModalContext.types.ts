export interface ModalContextProps {
  showModal: boolean
  toggleModal: (modal?: 'preReg' | 'partner') => void
  showFlyout: boolean
  toggleFlyout: () => void
}
