const typography = {
  paragraph: {
    1: {
      fontSizeMobile: '14px',
      lineHeightMobile: '20px',
      fontSize: '14px',
      lineHeight: '22px',
    },
    2: {
      fontSizeMobile: '16px',
      lineHeightMobile: '24px',
      fontSize: '14px',
      lineHeight: '22px',
    },
    3: {
      fontSizeMobile: '16px',
      lineHeightMobile: '24px',
      fontSize: '18px',
      lineHeight: '26px',
    },
  },
  heading: {
    1: {
      fontSizeMobile: '20px',
      lineHeightMobile: '28px',
      fontSize: '16px',
      lineHeight: '24px',
    },
    2: {
      fontSizeMobile: '24px',
      lineHeightMobile: '32px',
      fontSize: '32px',
      lineHeight: '44px',
    },
    3: {
      fontSizeMobile: '24px',
      lineHeightMobile: '38px',
      fontSize: '36px',
      lineHeight: '58px',
    },
    4: {
      fontSizeMobile: '30px',
      lineHeightMobile: '38px',
      fontSize: '52px',
      lineHeight: '60px',
    },
    5: {
      fontSizeMobile: '36px',
      lineHeightMobile: '44px',
      fontSize: '72px',
      lineHeight: '80px',
    },
  },
  fontWeight: {
    1: 100,
    2: 400,
    3: 600,
  },
  fontFamily: '-apple-system, BlinkMacSystemFont, Helvetica, Arial, sans-serif',
}

export default typography
