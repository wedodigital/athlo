import { Colours } from './colours.types'

const colours: Colours = {
  black: '#000000',
  white: '#ffffff',
  darkBlue: '#2c71b6',
  midBlue: '#29abe2',
  lightBlue: '#52aadd',
  lightGrey: '#f5f5f5',
  midGrey: '#595959',
  darkGrey: '#000915',
}

export default colours
