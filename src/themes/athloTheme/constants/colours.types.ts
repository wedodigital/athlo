export type colorsTypeKeys = 'black' | 'white' | 'lightBlue' | 'midBlue' | 'darkBlue' | 'lightGrey' | 'midGrey' | 'darkGrey'

export type Colours = {
  [key in colorsTypeKeys]: string
}
