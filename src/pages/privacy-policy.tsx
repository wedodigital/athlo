import React, { FC, ReactElement } from 'react'
import { graphql } from 'gatsby'

import PageFeature from '@components/PageFeature'
import FAQs from '@components/FAQs'
import CTABlock from '@components/CTABlock'
import HeadTags from '@components/HeadTags'
import Section from '@components/Section/Section'
import Container from '@components/Container/Container'
import ContentBlock from '@components/ContentBlock/ContentBlock'

interface HomepageData {
  data: {
    wpPage: {
      title: string
      content: string
      featuredImage: {
        node: {
          sourceUrl: string
        }
      }
      seo: {
        metaDesc: string
        metaKeywords: string
        canonical: string
        opengraphType: string
        opengraphUrl: string
        opengraphTitle: string
        opengraphSiteName: string
        opengraphPublisher: string
        opengraphPublishedTime: string
        opengraphModifiedTime: string
        opengraphImage: {
          sourceUrl: string
        }
        twitterTitle: string
        twitterDescription: string
        title: string
        twitterImage: {
          sourceUrl: string
        }
        schema: {
          articleType: string
          pageType: string
          raw: string
        }
      }
    }
  }
}

// markup
const PrivacyPage: FC<HomepageData> = ({ data }: HomepageData): ReactElement => {
  return (
    <>
      <HeadTags seo={data.wpPage.seo} />
      <PageFeature
        backgroundImage={data.wpPage.featuredImage.node.sourceUrl}
        title={data.wpPage.title}
      />
      <ContentBlock content={data.wpPage.content} />
      <CTABlock />
    </>
  )
}

export default PrivacyPage

export const privacyPage = graphql`
  query privacyPage {
    wpPage(databaseId: {eq: 3}) {
      title
      content
      featuredImage {
        node {
          sourceUrl
        }
      }
      seo {
        metaDesc
        metaKeywords
        canonical
        opengraphType
        opengraphUrl
        opengraphTitle
        opengraphSiteName
        opengraphPublisher
        opengraphPublishedTime
        opengraphModifiedTime
        opengraphImage {
          sourceUrl
        }
        twitterTitle
        twitterDescription
        title
        twitterImage {
          sourceUrl
        }
        schema {
          articleType
          pageType
          raw
        }
      }
    }
  }
`
