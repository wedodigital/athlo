import React, { FC, ReactElement } from 'react'
import { graphql } from 'gatsby'

import FeatureVideo from '@components/FeatureVideo'
import FeatureWorkings from '@components/FeatureWorkings'
import LargeImageSection from '@components/LargeImageSection'
import IconColumns from '@components/IconColumns'
import CTABlock from '@components/CTABlock'
import HeadTags from '@components/HeadTags'

interface HomepageData {
  data: {
    wpPage: {
      homepageContent: {
        appStoreUrl?: string
        googlePlayUrl?: string
        iconColumns: {
          content: string
          title: string
          icon: {
            sourceUrl: string
          }
        }[]
        mainFeatures: {
          content: string
          image: {
            sourceUrl: string
          }
          title: string
        }[]
        mainFeaturesSubtitle: string
        mainFeaturesTitle: string
        mainFeaturesTitleHighlight: string
        mp4: {
          sourceUrl: string
        }
        poster: {
          sourceUrl: string
        }
        secondaryFeatureSubtitle: string
        secondaryFeatureTitle: string
        secondaryFeatureTitleHighlight: string
        secondaryFeatureBackgroundImage: {
          sourceUrl: string
        }
        subtitle: string
        testimonials: {
          author: string
          testimonial: string
          image: {
            sourceUrl: string
          }
        }[]
        testimonialsTitle: string
        testimonialsTitleHighlight: string
        title: string
        titleHighlight: string
        webm: {
          id: string
          sourceUrl: string
          localFile: {
            id: string
          }
        }
      }
      seo: {
        metaDesc: string
        metaKeywords: string
        canonical: string
        opengraphType: string
        opengraphUrl: string
        opengraphTitle: string
        opengraphSiteName: string
        opengraphPublisher: string
        opengraphPublishedTime: string
        opengraphModifiedTime: string
        opengraphImage: {
          sourceUrl: string
        }
        twitterTitle: string
        twitterDescription: string
        title: string
        twitterImage: {
          sourceUrl: string
        }
        schema: {
          articleType: string
          pageType: string
          raw: string
        }
      }
    }
  }
}

// markup
const IndexPage: FC<HomepageData> = ({ data }: HomepageData): ReactElement => {
  const pageData = data.wpPage.homepageContent

  return (
    <>
      <HeadTags seo={data.wpPage.seo} />
      <FeatureVideo
        title={pageData.title}
        titleHighlight={pageData.titleHighlight}
        subTitle={pageData.subtitle}
        backgroundVideo={{
          webm: pageData.webm?.localFile.url,
          mp4: pageData.mp4?.localFile.url,
          poster: pageData.poster?.sourceUrl,
        }}
        appStore={pageData.appStoreUrl}
        googlePlay={pageData.googlePlayUrl}
      />
      <FeatureWorkings
        title={pageData.mainFeaturesTitle}
        titleHighlight={pageData.mainFeaturesTitleHighlight}
        subTitle={pageData.mainFeaturesSubtitle}
        features={pageData.mainFeatures}
      />
      <LargeImageSection
        title={pageData.secondaryFeatureTitle}
        subTitle={pageData.secondaryFeatureSubtitle}
        backgroundImage={pageData.secondaryFeatureBackgroundImage?.sourceUrl}
      />
      <IconColumns
        iconColumns={pageData.iconColumns}
      />
      <CTABlock />
    </>
  )
}

export default IndexPage

export const homepageQuery = graphql`
  query homePage {
    wpPage(databaseId: {eq: 2}) {
      homepageContent {
        appStoreUrl
        googlePlayUrl
        iconColumns {
          content
          title
          icon {
            sourceUrl
          }
        }
        mainFeatures {
          content
          image {
            localFile {
              childImageSharp {
                id
                fluid(maxWidth: 502) {
                  src
                }
              }
            }
          }
          title
        }
        mainFeaturesSubtitle
        mainFeaturesTitle
        mainFeaturesTitleHighlight
        mp4 {
          localFile {
            id
            url
          }
        }
        poster {
          sourceUrl
        }
        secondaryFeatureSubtitle
        secondaryFeatureTitle
        secondaryFeatureTitleHighlight
        secondaryFeatureBackgroundImage {
          sourceUrl
        }
        subtitle
        testimonials {
          author
          testimonial
          image {
            sourceUrl
          }
        }
        testimonialsTitle
        testimonialsTitleHighlight
        title
        titleHighlight
        webm {
          localFile {
            id
            url
          }
        }
      }
      seo {
        metaDesc
        metaKeywords
        canonical
        opengraphType
        opengraphUrl
        opengraphTitle
        opengraphSiteName
        opengraphPublisher
        opengraphPublishedTime
        opengraphModifiedTime
        opengraphImage {
          sourceUrl
        }
        twitterTitle
        twitterDescription
        title
        twitterImage {
          sourceUrl
        }
        schema {
          articleType
          pageType
          raw
        }
      }
    }
  }
`
