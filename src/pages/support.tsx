import React, { FC, ReactElement } from 'react'
import { graphql } from 'gatsby'

import PageFeature from '@components/PageFeature'
import FAQs from '@components/FAQs'
import CTABlock from '@components/CTABlock'
import HeadTags from '@components/HeadTags'

interface HomepageData {
  data: {
    wpPage: {
      title: string
      supportPage: {
        backgroundImage: {
          sourceUrl: string
        }
        faqs: {
          question: string
          answer: string
        }[]
        introText: string
      }
      seo: {
        metaDesc: string
        metaKeywords: string
        canonical: string
        opengraphType: string
        opengraphUrl: string
        opengraphTitle: string
        opengraphSiteName: string
        opengraphPublisher: string
        opengraphPublishedTime: string
        opengraphModifiedTime: string
        opengraphImage: {
          sourceUrl: string
        }
        twitterTitle: string
        twitterDescription: string
        title: string
        twitterImage: {
          sourceUrl: string
        }
        schema: {
          articleType: string
          pageType: string
          raw: string
        }
      }
    }
  }
}

// markup
const SupportPage: FC<HomepageData> = ({ data }: HomepageData): ReactElement => {
  const pageData = data.wpPage.supportPage

  return (
    <>
      <HeadTags seo={data.wpPage.seo} />
      <PageFeature
        backgroundImage={pageData.backgroundImage.sourceUrl}
        title={data.wpPage.title}
        introText={pageData.introText}
      />
      <FAQs
        faqs={pageData.faqs}
      />
      <CTABlock />
    </>
  )
}

export default SupportPage

export const supportPage = graphql`
  query supportPage {
    wpPage(databaseId: {eq: 139}) {
      title
      supportPage {
        backgroundImage {
          sourceUrl
        }
        faqs {
          question
          answer
        }
        introText
      }
      seo {
        metaDesc
        metaKeywords
        canonical
        opengraphType
        opengraphUrl
        opengraphTitle
        opengraphSiteName
        opengraphPublisher
        opengraphPublishedTime
        opengraphModifiedTime
        opengraphImage {
          sourceUrl
        }
        twitterTitle
        twitterDescription
        title
        twitterImage {
          sourceUrl
        }
        schema {
          articleType
          pageType
          raw
        }
      }
    }
  }
`
