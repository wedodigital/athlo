import React, { FC, ReactElement } from 'react'
import { graphql } from 'gatsby'

import PageFeature from '@components/PageFeature'
import GymGrid from '@components/GymGrid'
import CTABlock from '@components/CTABlock'
import HeadTags from '@components/HeadTags'

interface HomepageData {
  data: {
    wpPage: {
      title: string
      partnerPage: {
        backgroundImage: {
          sourceUrl: string
        }
        faqs: {
          question: string
          answer: string
        }[]
        introText: string
      }
      seo: {
        metaDesc: string
        metaKeywords: string
        canonical: string
        opengraphType: string
        opengraphUrl: string
        opengraphTitle: string
        opengraphSiteName: string
        opengraphPublisher: string
        opengraphPublishedTime: string
        opengraphModifiedTime: string
        opengraphImage: {
          sourceUrl: string
        }
        twitterTitle: string
        twitterDescription: string
        title: string
        twitterImage: {
          sourceUrl: string
        }
        schema: {
          articleType: string
          pageType: string
          raw: string
        }
      }
    }
  }
}

// markup
const SupportPage: FC<HomepageData> = ({ data }: HomepageData): ReactElement => {
  const pageData = data.wpPage.partnerContent

  return (
    <>
      <HeadTags seo={data.wpPage.seo} />
      <PageFeature
        title={data.wpPage.title}
        introText={pageData.introText}
      />
      <GymGrid
        partners={pageData.partnerGrid}
      />
      <If condition={pageData.comingSoon}>
        <GymGrid
          partners={pageData.comingSoon}
          heading='Upcoming Partners'
          columns={6}
        />
      </If>
      <CTABlock />
    </>
  )
}

export default SupportPage

export const partnersPage = graphql`
  query partnerPage {
    wpPage(databaseId: {eq: 197}) {
      title
      partnerContent {
        backgroundImage {
          sourceUrl
        }
        introText
        partnerGrid {
          logo {
            sourceUrl
          }
          name
          url
        }
        comingSoon {
          logo {
            sourceUrl
          }
          name
          url
        }
      }
      seo {
        metaDesc
        metaKeywords
        canonical
        opengraphType
        opengraphUrl
        opengraphTitle
        opengraphSiteName
        opengraphPublisher
        opengraphPublishedTime
        opengraphModifiedTime
        opengraphImage {
          sourceUrl
        }
        twitterTitle
        twitterDescription
        title
        twitterImage {
          sourceUrl
        }
        schema {
          articleType
          pageType
          raw
        }
      }
    }
  }
`
