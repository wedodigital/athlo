import React, { PureComponent, ReactNode } from 'react'
import { ThemeProvider } from 'styled-components'

import GlobalStyle from '@styles/GlobalStyle'
import { athloTheme } from '@themes/athloTheme'

import ModalContext from '@context/ModalContext'

import Header from '@components/Header'
import Footer from '@components/Footer'
import Modal from '@components/Modal'
import Flyout from '@components/Flyout'
import PreRegistration from '@components/PreRegistration'
import GymPartner from '@components/GymPartner'

import { PageLayoutProps, PageLayoutState } from './PageLayout.types'

export default class PageLayout extends PureComponent<PageLayoutProps, PageLayoutState> {
  public state: PageLayoutState = {
    showModal: false,
    showFlyout: false,
  }

  private toggleModal = (modal?) => {
    this.setState({
      showModal: !this.state.showModal,
      modalToShow: modal,
    })
  }

  private toggleFlyout = () => {
    this.setState({
      showFlyout: !this.state.showFlyout,
    })
  }

  render(): ReactNode {
    return (
      <ThemeProvider theme={athloTheme}>
        <GlobalStyle />
        <ModalContext.Provider
          value={{
            showModal: this.state.showModal,
            toggleModal: this.toggleModal,
            showFlyout: this.state.showFlyout,
            toggleFlyout: this.toggleFlyout,
          }}
        >
          <Header />
          <Flyout isOpen={this.state.showFlyout} />
          <If condition={this.state.showModal}>
            <Modal>
              <Choose>
                <When condition={this.state.modalToShow === 'preReg'}>
                  <PreRegistration />
                </When>
                <When condition={this.state.modalToShow === 'partner'}>
                  <GymPartner />
                </When>
              </Choose>
            </Modal>
          </If>
          {this.props.children}
          <Footer />
        </ModalContext.Provider>
      </ThemeProvider>
    )
  }
}
