import { ReactNode } from 'react'

export interface PageLayoutProps {
  children: ReactNode
}

export interface PageLayoutState {
  showModal: boolean
  showFlyout: boolean
  modalToShow?: 'preReg' | 'partner'
}
